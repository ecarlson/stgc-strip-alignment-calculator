import sqlite3 as sql
import os
import Tools as my
import math


class DBManager(object):
    def __init__(self, dbFileName='parenting.db'):
        self.dbFile=dbFileName
        self.open=False
        if not my.FileExists(self.dbFile):
            print('Creating DB file: ', self.dbFile)
            self.initialized = False
        else:
            print('DB file already exists, will not delete: ', self.dbFile)
            self.initialized = True

    def Open(self):
        self.conn = sql.connect(self.dbFile)
        self.cursor = self.conn.cursor()
        self.open=True

    def GetCursor(self):
        return self.cursor
        
    def Close(self):
        self.cursor.close()
        self.open=False
        
    def IsOpen(self):
        return self.open
    

class ParentingDBManager(DBManager):
    def __init__(self, dbFileName='parenting.db'):
        DBManager.__init__(self, dbFileName)
        if not self.initialized:
            self.InitDB()
        
    def InitDB(self):
        conn = sql.connect(self.dbFile)
        c = conn.cursor()
        c.execute('CREATE TABLE parenting ( quad_type TEXT NOT NULL, quad_sn INTEGER NOT NULL, layer INTEGER NOT NULL, board_sn INTEGER, PRIMARY KEY ( quad_type, quad_sn, layer ) )')
        conn.commit()
        c.close()
        self.initialized=True
        
    def AddBoard(self, quadType, quadSN, layer, boardSN=-1):
        if self.EntryExists(quadType, quadSN, layer):
            currentSN = self.GetBoardSN(quadType, quadSN, layer)
            if int(currentSN) != int(boardSN):
                print("WARNING: Will change SN of board ('%s', '%i', '%i') from SN '%i' to '%i'" % (quadType, quadSN, layer, currentSN, boardSN))
                self.UpdateBoard(quadType, quadSN, layer, boardSN)
            return False
        else:
            self.InsertBoard(quadType, quadSN, layer, boardSN)
            return True

    def UpdateBoard(self, quadType, quadSN, layer, boardSN=-1):
        command = "UPDATE parenting SET board_sn = '%i' WHERE quad_type='%s' AND quad_sn='%i' AND layer='%i'" % (int(boardSN), quadType, int(quadSN),layer)
        self.cursor.execute(command)
        self.conn.commit()

    def InsertBoard(self, quadType, quadSN, layer, boardSN=-1):
        command = "INSERT INTO parenting (quad_type, quad_sn, layer, board_sn) VALUES ( '%s', '%i', '%i', '%i' )" % (quadType, quadSN, layer, boardSN)
        self.cursor.execute(command)
        self.conn.commit()


    def SearchBoards(self, quadType, quadSN, layer):
        command = "SELECT board_sn FROM parenting WHERE quad_type='%s' AND quad_sn='%i' AND layer='%i'" % (quadType, int(quadSN), int(layer))
        self.cursor.execute(command)
        return self.cursor.fetchall()

    
    def EntryExists(self, quadType, quadSN, layer):
        res = self.SearchBoards(quadType, quadSN, layer)
        nBoards = len(res)
        if nBoards > 1:
            print("WARNING: More than one board found for keys: '%s', '%i', '%i'" % (quadType, int(quadSN), int(layer)))

        return nBoards>=1

    # Run a search for a board, if no entry or more than one return -1
    def GetBoardSN(self, quadType, quadSN, layer):
        res = self.SearchBoards(quadType, quadSN, layer)
        if len(res)>1 or len(res)==0:
            return -1
        else:
            return int(res[0][0])


class QCDBManager(DBManager):
    def __init__(self, dbFileName='qc.db'):
        DBManager.__init__(self, dbFileName)
        if not self.initialized:
            self.InitDB()

    def InitDB(self):
        conn = sql.connect(self.dbFile)
        c = conn.cursor()
        c.execute('CREATE TABLE qc ( quad_type TEXT NOT NULL, board_type TEXT NOT NULL,  board_sn INTEGER NOT NULL, scale REAL, nonpar REAL, offset REAL, angle REAL, PRIMARY KEY ( quad_type, board_type, board_sn ) )')
        conn.commit()
        c.close()
        self.initialized=True


    def AddBoard(self, quadType, boardType, sn, scale, nonpar, offset, angle):
        if self.EntryExists(quadType, boardType, sn):
            self.UpdateBoard(quadType, boardType, sn, scale, nonpar, offset, angle)
            return False
        else:
            self.InsertBoard(quadType, boardType, sn, scale, nonpar, offset, angle)
            return True


    def EntryExists(self, quadType, boardType, sn):
        res = self.SearchBoards(quadType, boardType, sn)
        nBoards = len(res)
        if nBoards > 1:
            print("WARNING: More than one board found for keys: '%s', '%s', '%i'" % (quadType, boardType, sn))

        return nBoards>=1

    def SearchBoards(self, quadType, boardType, boardSN):
        command = "SELECT scale, nonpar, offset, angle FROM qc WHERE quad_type='%s' AND board_type='%s' AND board_sn='%i'" % (quadType, boardType, boardSN)
        self.cursor.execute(command)
        return self.cursor.fetchall()


    def UpdateBoard(self, quadType, boardType, sn, scale, nonpar, offset, angle):
        command = "UPDATE qc SET scale = '%f', nonpar = '%f', offset = '%f', angle = '%f' WHERE quad_type='%s' AND board_type='%s' AND board_sn='%i'" % (my.CFlt(scale), my.CFlt(nonpar), my.CFlt(offset), my.CFlt(angle), quadType, boardType, sn)
        self.cursor.execute(command)
        self.conn.commit()

    def InsertBoard(self, quadType, boardType, sn, scale, nonpar, offset, angle):
        command = "INSERT INTO qc (quad_type, board_type, board_sn, scale, nonpar, offset, angle) VALUES ('%s', '%s', '%i', '%f', '%f', '%f', '%f')" % (quadType, boardType, sn, my.CFlt(scale), my.CFlt(nonpar), my.CFlt(offset), my.CFlt(angle))
        self.cursor.execute(command)
        self.conn.commit()


    # Run a search for a board, if no entry or more than one return nan for all values
    def GetQCMeas(self, quadType, boardType, boardSN):
        vals = {'scale': math.nan, 'nonpar': math.nan, 'offset': math.nan, 'angle': math.nan}
        res = self.SearchBoards(quadType, boardType, boardSN)
        if len(res)>1 or len(res)==0:
            return vals
        else:
            resSingle = res[0]
            vals['scale'] = resSingle[0]
            vals['nonpar'] = resSingle[1]
            vals['offset'] = resSingle[2]
            vals['angle'] = resSingle[3]
            vals['anglerad'] = vals['angle']*math.pi/180.
            return vals


# Checks that the dict of qc values has the rights keys
# and if none of the values are NaN
def ValidateQCValues(vals):
    expectedKeys = ['scale', 'nonpar', 'offset', 'angle', 'anglerad'];
    for expectedKey in expectedKeys:
        if expectedKey not in vals:
            return False
        if vals[expectedKey]==math.nan:
            return False
    return True
