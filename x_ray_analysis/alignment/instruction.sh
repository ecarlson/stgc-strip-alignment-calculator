#!/bin/bash

# Populate the parenting DB
# You will need to clone the QA/QC spreadsheet repository
# URL of gitlab group: https://gitlab.cern.ch/atlas-muon-nsw-db 
# Example for sTGC-QC-S1 shown below:
cd
mkdir atlas-muon-nsw-db
cd atlas-muon-nsw-db
git clone ssh://git@gitlab.cern.ch:7999/atlas-muon-nsw-db/sTGC-QC-S1.git --branch XML2Excel --single-branch
cd ../alignment/
python3 GetParenting.py ~/atlas-muon-nsw-db/sTGC-QC-S1

# Populate the QC db
# You will need to have all 'General Summary' spreadsheet copied locally
# The format of those spreadsheets will have to be worked on a little
# Example for type QS3 shown below:
python3 GetQCMeasurements.py S3


# Run the calculator
# Need the quad type (including P or C) and serial number of quad
# Example for quad type QS3P and ID 16 shown below
python3 main.py QS3P 6

# PDF file QS3P_SN6.pdf was created

