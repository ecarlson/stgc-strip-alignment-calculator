import os
import math

# Check whether the specified directory is valid
def IsValidDir(dir):
    return os.path.isdir(dir) and os.path.exists(dir)

def FileExists(fileName):
    return os.path.exists(fileName)

def IsValidQuadType(type):
    return type in ['QS1', 'QS2', 'QS3', 'QL1', 'QL2', 'QL3']

def CFlt(valStr):
    try:
       valF = float(valStr)
    except Exception:
        valF = math.nan

    return valF

def IsValidTest(test):
    if type(test) == bool:
        return True
    else:
        raise Exception("Invalid test! Please enter True or False.")


def IsValidSubQuadType(type):
    if len(type)!=4 or type[0]!='Q':
        return False

    quadSize = type[1]
    if not (quadSize=='L' or quadSize=='S'):
        return False

    quadID = int(type[2])
    if quadID<1 or quadID>3:
        return False

    logic = type[3]
    if not (logic=='P' or logic=='C'):
        return False

    return True

def IsValidCoords(coords):
    if not (coords == 'Wedge' or coords == 'Pin'):
        return False

    return True 

def IsValidCSVName(name):
    if '.csv' in name:
        return True

    return False

def IsValidRootName(name):
    if '.root' in name:
        return True
    return False

def IsGoodDQ(dq_flag):
    if (dq_flag == 'BAD_CHANNEL') or (dq_flag == 'WARNING_BAD_SHAPE') or (dq_flag == 'WIRESUPPORT'):
        return False
    else:
        return True

def IsOkDQ(dq_flag):
    if (dq_flag == 'OK'):
        return True
    else:
        return False