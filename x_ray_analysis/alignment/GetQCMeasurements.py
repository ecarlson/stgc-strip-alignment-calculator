#!/usr/local/bin/python3

# Parses the "General Summary" spreadsheets that contain CMM
# measurements (scale, non-par, offset, angle) of cathode boards
# and saves to a SQLite database.
#
# The spreadsheets were downloaded from:
# URL: https://drive.google.com/drive/u/0/folders/0B5w6KDfPnxLiRGZIMkd5ZWtvVzA

import argparse
import os
import Tools as my
from openpyxl import load_workbook
from DBManager import QCDBManager


def GetRowsIndex(sh, rowIdx):
    for rowName in rowIdx.keys():
        for row in range(1, 30):
            if sh.cell(row=row, column=1).value==rowName:
                rowIdx[rowName] = row
                break
        if rowIdx[rowName]<=0:
            print("Cannot find location of row: "+rowName)

    return rowIdx


# Arguments parsers
def ParseInput():
    parser = argparse.ArgumentParser(description='Get CMM measurements from "General Summary" spreadsheets')
    parser.add_argument('quad_type', action='store', help='Module type (S1,S2,S3,L1,L2,L3)')
    return parser.parse_args()

# Main function
if __name__ == "__main__":
    print("**** Get QC measurements ****")

    args = ParseInput()
    quadType = args.quad_type

    excelFileName = os.path.join('qc_spreadsheets', 'G'+quadType+" General Summary.xlsx")
    if not my.FileExists(excelFileName):
        raise Exception('QC measurements excel file not found: '+excelFileName)


    db = QCDBManager()
    db.Open()    
    
    print('Processing file: ', excelFileName)
    shList = ['P13', 'P24']
    shList = [('G'+quadType+shName) for shName in shList]
    wb = load_workbook(excelFileName)
    insertCount=0
    
    for shName in shList:
        boardType = shName[-2:]
        try:
            sh = wb[shName]
            rowIdx = {"Serial Number": -1, "Scale": -1, "Non parallelism": -1, "Offset": -1, "Angle": -1}
            rowIdx = GetRowsIndex(sh, rowIdx)

            col=2
            colSNVal = sh.cell(row=rowIdx["Serial Number"], column=col).value
            while colSNVal:
                snStr = colSNVal[-4:]
                if(snStr.isdigit()):
                    sn=int(snStr)
                    scale = sh.cell(row=rowIdx["Scale"], column=col).value
                    nonpar = sh.cell(row=rowIdx["Non parallelism"], column=col).value
                    offset = sh.cell(row=rowIdx["Offset"], column=col).value
                    angle = sh.cell(row=rowIdx["Angle"], column=col).value
                
                    if db.AddBoard('Q'+quadType, boardType, sn, scale, nonpar, offset, angle):
                        insertCount = insertCount + 1
                else:
                    print("WARNING: No serial number found in:", colSNVal)
                
                col = col+1
                colSNVal = sh.cell(row=rowIdx["Serial Number"], column=col).value
            
        except Exception as ex:
            print(ex)
            print('Cannot read sheet '+shName+' from workbook '+excelFileName)


    print('Number of inserted boards:', insertCount)         
    db.Close()

    
