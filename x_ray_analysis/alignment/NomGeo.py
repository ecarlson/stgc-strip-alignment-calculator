# Container for the nominal geometry of a quad
# Get the data from the SQLite database

import configparser


# Converts text to float
# May return NaN if invalid
def IsFloat(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def ProcessTextF(text):
    if IsFloat(text):
        return float(text)
    else:
        return math.nan

def ProcessTextI(text):
    if text.isdigit():
        return int(text)
    else:
        return -1

# This class may define the nominal geometry of a quadruplet
class NomQuadGeo(object):
    configFileName = 'NominalGeometry.ini'

    def __init__(self, type):
        self.type = type
        f = configparser.ConfigParser()
        f.read(NomQuadGeo.configFileName)
        if not type in f.sections():
            raise Exception('Cannot find type '+type+' in config.');

        geof = f[type]
        self.nStrips = ProcessTextI(geof['nStrips'])
        self.h = ProcessTextF(geof['h'])
        self.a = ProcessTextF(geof['a'])
        self.boardAngle = ProcessTextF(geof['boardAngle'])

        self.b = dict()
        self.f = dict()
        self.alpha = dict()
        self.middleIndex = dict()
        self.x0 = dict()

        
        for t in ['K13', 'K24']:
            self.b[t] = ProcessTextF(geof['b_'+t])
            self.f[t] = ProcessTextF(geof['f_'+t])
            self.alpha[t] = ProcessTextF(geof['alpha_'+t])
            self.middleIndex[t] = ProcessTextI(geof['middleIndex_'+t])
            self.x0[t] = ProcessTextF(geof['x0_'+t])

        self.yMin = ProcessTextF(geof['yMin'])
        self.yMax = ProcessTextF(geof['yMax'])
        self.xMin = ProcessTextF(geof['xMin'])
        self.xMax = ProcessTextF(geof['xMax'])

        self.angleStart = ProcessTextF(geof['angleStart'])

        self.z = dict()
        for l in range(1, 5):
            self.z[l] = ProcessTextF(geof['z'+str(l)])

        self.fixture_min = ProcessTextI(geof['fixture_min'])
        self.fixture_max = ProcessTextI(geof['fixture_max'])

        self.wedge_y = ProcessTextF(geof['wedge_y'])

    
    def __str__(self):
        s = ''
        s += ("Quadruplet type: " + self.type+'\n')
        s += ("From config file: " + str(NomQuadGeo.configFileName)+'\n')
        s += ("nStrip=" + str(self.nStrips)+'\n')
        s += ("h="+ str(self.h) + '\n')
        s += ("a="+ str(self.a) + '\n')
        s += ("boardAngle=" + str(self.boardAngle)+'\n')
        for t in self.b.keys():
            s += ("b_"+t+"="+ str(self.b[t])+'\n')
            s += ("f_"+t+"="+ str(self.f[t])+'\n')
            s += ("alpha_"+t+"="+ str(self.alpha[t])+'\n')
            s += ("middleIndex_"+t+"="+ str(self.middleIndex[t])+'\n')
            s += ("x0_"+t+"="+ str(self.x0[t])+'\n')
            
        s += ("yMin="+ str(self.yMin)+'\n')
        s += ("yMax="+ str(self.yMax)+'\n')
        s += ("xMin="+ str(self.xMin)+'\n')
        s += ("xMax="+ str(self.xMax)+'\n')

        s += ("angleStart="+ str(self.angleStart)+'\n')
        s += ("wedge_y = " + str(self.wedge_y) + '\n')

        for l, z in self.z.items():
            s += ('z'+str(l)+'='+str(z)+'\n')
            
        return s
              

def Layer2StripBoardType(layer):
    if layer==1 or layer==3:
        return 'K13'
    elif layer==2 or layer==4:
        return 'K24'
    else:
        raise Exception('Invalid layer')
    
# Represent the geometry of a doublet
# Object created from the quad geometry and specifying a pair of layers
# The doublet layers are indexed 
class NomDoubGeo(object):
    def __init__(self, quad_geo, layers_pair):

        # Values inherited from the quad geometry
        self.boardAngle = quad_geo.boardAngle
        self.h = quad_geo.h
        self.a = quad_geo.a
        self.nStrips = quad_geo.nStrips

        # Logic checks
        if len(layers_pair)!=2:
            raise Exception('A doublet must have 2 layers!')
        if layers_pair[0]==layers_pair[1]:
            raise Exception('A doublet must be made up of different layers of a quad!')
        
        # Values specific to L1 and L2 of doublet
        self.alpha = dict()
        self.b = dict()
        self.f = dict()
        self.middleIndex = dict()
        self.x0 = dict()
        self.z = dict()

        for l in range(1,3): # doublets have layers indexed 1 and 2
            quadL = layers_pair[l-1]
            bType = Layer2StripBoardType(quadL)
            self.alpha[l] = quad_geo.alpha[bType]
            self.b[l] = quad_geo.b[bType]
            self.f[l] = quad_geo.f[bType]
            self.middleIndex[l] = quad_geo.middleIndex[bType]
            self.x0[l] = quad_geo.x0[bType]
            self.z[l] = quad_geo.z[quadL]    

# Geometry of a single layer
# Must specify the layer at the doublet level (1 or 2)
class NomBoardGeo(object):
    def __init__(self, doub_geo, doub_layer):

        # Values inherited from the quad/doublet geometry
        self.boardAngle = doub_geo.boardAngle
        self.h = doub_geo.h
        self.a = doub_geo.a
        self.nStrips = doub_geo.nStrips

        # Logic check
        if not doub_layer in [1,2]:
            raise Exception('Invalid layer: '+doub_layer)

        self.alpha = doub_geo.alpha[doub_layer]
        self.b = doub_geo.b[doub_layer]
        self.f = doub_geo.f[doub_layer]
        self.middleIndex = doub_geo.middleIndex[doub_layer]
        self.x0 = doub_geo.x0[doub_layer]
        self.z = doub_geo.z[doub_layer]
        
