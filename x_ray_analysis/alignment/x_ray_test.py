import argparse
import csv
from NomGeo import NomQuadGeo
import DBManager
from DBManager import QCDBManager, ParentingDBManager
import math
from quadruplet import quadruplet
from doublet import doublet
from board import board
import x_ray_point
import Tools as my
from matplotlib.image import NonUniformImage
import matplotlib.pyplot as plt
# import pandas as pd
from openpyxl import load_workbook
from matplotlib.backends.backend_pdf import PdfPages
from MisalignmentPlotter import MisalignmentPlotter
import shutil


# Arguments parsers
def ParseInput():
    parser = argparse.ArgumentParser(description='sTGC misalignment calculator')
    parser.add_argument('quad_type',  action='store', help='Quadruplet type. Possible choices: Q{S,L}{1,2,3}{P,C}')
    parser.add_argument('SN',  action='store', help='Serial\# of module.')
    parser.add_argument('input_file', action='store', help='Name of the xlsm file containing the x-ray measurements.')
    parser.add_argument('--output', action='store', help='PDF output. default=o.pdf')
    parser.add_argument('--pred_csv', action = 'store', help='CSV output for predictions. default=quad_type+quadSN + _pred.csv')
    parser.add_argument('--output_csv', action = 'store', help = 'CSV output for both predictions and measurements. default=quad_type+quadSN + .csv')
    return parser.parse_args()

# Main function
if __name__ == "__main__":

    # Start program / parse arguments
    print('**** sTGC misalignment calculator ****')
    args = ParseInput()
    quadType = args.quad_type
    if not my.IsValidSubQuadType(quadType):
        raise Exception("Invalid quadruplet type!")

    subQuadType = quadType[0:3]
    quadSN = args.SN
    measurement_file = args.input_file
    file_split = measurement_file.split('.')
    print(measurement_file)
    measurement_file_w_pred = file_split[0] + "_pred.xlsx"

    # The coordinate system for x-ray measurements is always wedge, so no need to include it as an argument.
    coords = 'Wedge'

    if args.output:
        outPDF = args.output
    else:
        outPDF = quadType+'_SN'+str(quadSN)+'.pdf'

    # Put the results into a subdirectory to avoid clutter.
    comparison_directory = 'comparison_csv/'
    prediction_directory = 'quadruplet_predicted_misalignments/'

    # If the user included a file name, make sure that it is a valid csv name, else set it to the default.
    if args.pred_csv:
        if my.IsValidCSVName(args.pred_csv):
            csv_filename = prediction_directory + args.pred_csv
        else:
            raise Exception("Invalid CSV file name!")
    else:
        csv_filename = prediction_directory + quadType + quadSN + '_pred.csv'
    # Same as previous block.
    if args.output_csv:
    	if my.IsValidCSVName(args.output_csv):
    		output_filename = comparison_directory + args.output_csv
    	else:
    		raise Exception("Invalid CSV file name!")
    else:
        output_filename = comparison_directory + quadType + quadSN + '.csv'

    # Print out the quad being tested as well as the output files.
    print('Quadruplet Type: ', quadType)
    print("Serial Number: ", quadSN)
    print('PDF Output: ', outPDF)
    print('Prediction Output: ', csv_filename)
    print('Comparison Output: ', output_filename)



    # Get nominal geo
    nomQuadGeo = NomQuadGeo(subQuadType)
    
    # Get parenting
    dbParenting = ParentingDBManager()
    dbParenting.Open()
    boardsSN = dict()
    for layer in range(1,5):
        sn = dbParenting.GetBoardSN(quadType, quadSN, layer)
        if sn<0:
            raise Exception('Parenting for '+quadType+' SN '+quadSN+' not found or not complete.')
        else:
            boardsSN[layer]=sn
    
    print('Strip cathode boards parenting:', boardsSN)
    dbParenting.Close()
    
    # Get QA/QC measurements
    dbQC = QCDBManager()
    dbQC.Open()
    boardsQC = dict()
    for layer in range(1,5):
        if layer==1 or layer==3:
            boardType = '13'
        elif layer==2 or layer==4:
            boardType = '24'
        else:
            boardType = ''

        thisqc = dbQC.GetQCMeas(subQuadType, boardType, boardsSN[layer])
        if not DBManager.ValidateQCValues(thisqc):
            raise Exception('Invalid QC measurements for board ' + str(subQuadType) + ' ' + str(boardType) + ' ' + str(boardsSN[layer]))
        else:
            boardsQC[layer] = thisqc        
        
    dbQC.Close()
    print('QC measurements:', boardsQC)
    
    # Construct detector objects
    print('Initializing quadruplet object...')
    quad = quadruplet(subQuadType, nomQuadGeo, boardsQC, coords)
    print(subQuadType)

    # If comparing to x-ray results, construct the dictionary of points that will be measured. Open the appropriate excel file for the points
    x_ray_results = x_ray_point.quad_x_ray_results(quad, quadType, csv_filename)
    measurements, measurement_errors = x_ray_point.parse_measurements(measurement_file)
    x_ray_point.WriteToCSV(x_ray_results, measurements, measurement_errors, output_filename)
    print("\nENTERING THE WRITE PREDICTION COLUMNS FUNCTION\n")
    x_ray_point.WritePredictionColumns(measurement_file,measurement_file_w_pred,x_ray_results,measurements,measurement_errors)

    print('**** Goodbye! ****')