# Quadruplet misalignments at multiple locations are computed in this module

import numpy as np
from quadruplet import quadruplet
from matplotlib.image import NonUniformImage
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import math

# Return 2 layers out of 4 which are not l1 or L2
def GetExclusiveLayers(l1, l2):
    l1Ex=l2Ex=-1
    for l in range(1,5):
        if l not in [l1, l2]:
            if l1Ex<0:
                l1Ex=l
            elif l2Ex<0:
                l2Ex=l

    if l1Ex<0 or l2Ex<0:
        raise Exception('Failed to find exclusive layers.')

    return l1Ex, l2Ex

    

# Class that calculates the misalignement and can generate plots
# of the results. Only need a quadruplet as input.
class MisalignmentPlotter(object):
    def __init__(self, quad, pdfOutput='out.pdf'):
        self.quad=quad
        self.pdfOutput=pdfOutput
        #self.pdf = PdfPages(self.pdfOutput)
        self.currentFigIndex = 0
        self.figIndexList = []

        
#    def SetPDFOutput(self, output):
#        self.pdfOutput = output
#        self.pdf = PdfPages(self.pdfOutput)

        
#    def PrintAllFigure(self):
#        print('Printing', len(self.figIndexList), 'figures to PDF...')
#        with PdfPages(self.pdfOutput) as pdf:
#            for figIndex in self.figIndexList:
#                print('Printing index', figIndex, '/', len(self.figIndexList))
#                plt.figure(figIndex)
#                pdf.savefig()
                
                
    def ComputeMisalignmentsAllLayersCombinations(self, nBinsX, nBinsY, nPoints):
        print('Computing misalignments for all reference layers combinations...')
        for l1RefIdx in range(1,4):
            for l2RefIdx in range(l1RefIdx + 1,5):
                print('\tProcessing', 'lRef1='+str(l1RefIdx), 'lRef2='+str(l2RefIdx), '...')
                self.ComputeMisalignments(l1RefIdx, l2RefIdx, nBinsX, nBinsY, nPoints)

                
    # Misalignments obtained in that function
    def ComputeMisalignments(self, firstRefLayerIdx, secondRefLayerIdx, nBinsX, nBinsY, nPoints):
        g=self.quad.geo_nom
        xMax, xMin, yMax, yMin = g.xMax, g.xMin, g.yMax, g.yMin
        
        # Get misaligned layers given reference
        firstAlignLayerIdx, secondAlignLayerIdx = GetExclusiveLayers(firstRefLayerIdx, secondRefLayerIdx)
        refLayers = [firstRefLayerIdx, secondRefLayerIdx]
        alignLayers = [firstAlignLayerIdx, secondAlignLayerIdx]

        
        # Declare 2D data structures to store results (for align layers 1 and 2)
        avgMisalignFirst = np.zeros(shape=(nBinsY, nBinsX))
        avgMisalignErrFirst = np.zeros(shape=(nBinsY, nBinsX))
        avgMisalignSecond = np.zeros(shape=(nBinsY, nBinsX))
        avgMisalignErrSecond = np.zeros(shape=(nBinsY, nBinsX))

        
        # Limits of bins
        binLimitsX = np.linspace(xMin, xMax, nBinsX + 1)
        binLimitsY = np.linspace(yMin, yMax, nBinsY + 1)
        
        # Loop through bins (numbered from 0 to N-1)
        for binX in range(0, nBinsX):
            for binY in range(0, nBinsY):
                coordinates_x = np.linspace(binLimitsX[binX], binLimitsX[binX+1], nPoints)
                coordinates_y = np.linspace(binLimitsY[binY], binLimitsY[binY+1], nPoints)

                dFirstVec=[]
                dErrFirstVec=[]
                dSecondVec=[]
                dErrSecondVec=[]
                ptCount=0
                for x in coordinates_x:
                    for y in coordinates_y:
                        if self.quad.InTrapezoid(x,y):
                            residuals, residualsError = self.quad.track_misalignments(x, y, refLayers, alignLayers)
                            dFirstVec.append(residuals[0])
                            dErrFirstVec.append(residualsError[0])
                            dSecondVec.append(residuals[1])
                            dErrSecondVec.append(residualsError[1])
                            ptCount+=1


                # Average results from all space points (if any point at all in bin)
                if ptCount>pow(nPoints,2)/2.:
                    avgMisalignFirst[binY][binX] = 1000*np.mean(dFirstVec)
                    avgMisalignErrFirst[binY][binX] = 1000*np.mean(dErrFirstVec)
                    avgMisalignSecond[binY][binX] = 1000*np.mean(dSecondVec)
                    avgMisalignErrSecond[binY][binX] = 1000*np.mean(dErrSecondVec)
                else:
                    print("Bin " + str(binX) + "X " + str(binY) + "Y is empty")
                
        refLTitle = ' Reference Layers: L'+str(refLayers[0])+str(refLayers[1])+') '
        title ='Relative Misalignments [Microns]'+refLTitle+'- Layer '
        self.MakeAlignPlot(avgMisalignFirst, avgMisalignErrFirst, binLimitsX, binLimitsY, title+str(firstAlignLayerIdx))
        self.MakeAlignPlot(avgMisalignSecond, avgMisalignErrSecond, binLimitsX, binLimitsY, title+str(secondAlignLayerIdx))


    # Make the 2D alignment plots
    # Returns the formatted matplotlib plot
    def MakeAlignPlot(self, vals, error, binLimitsX, binLimitsY, title):
        print('\tPlotting:', title)
        
        # Deduce bin data
        nBinsX, nBinsY = len(binLimitsX)-1, len(binLimitsY)-1 
        limits = [binLimitsX[0], binLimitsX[-1], binLimitsY[0], binLimitsY[-1]]

        # Format and labels
#        plt.figure(num=self.currentFigIndex, figsize = (12,6))
        plt.figure(figsize = (12,6))
 #       self.figIndexList.append(self.currentFigIndex)
 #       self.currentFigIndex += 1
        plt.rc('text', usetex = True)
        plt.rc('font', family = 'serif')
        plt.imshow(vals, cmap='rainbow', origin='lower', extent=limits, aspect=0.8)
        plt.ylabel('Y Coordinate [mm]')
        plt.xlabel('X Coordinate [mm]')
        plt.title(title)

        # Write labels on bins
        for binX in range (0, nBinsX):
            for binY in range(0, nBinsY):
                hBinY = binLimitsY[binY+1]-binLimitsY[binY]
                x_text_coord = binLimitsX[binX+1] - 0.75*(binLimitsX[binX+1]-binLimitsX[binX])
                y_text_coord = (binLimitsY[binY+1]+binLimitsY[binY])/2.
                misalignment_string = "{0:.2f}".format(vals[binY][binX])
                error_string = "{0:.2f}".format(error[binY][binX])
                plt.text(x_text_coord, y_text_coord, misalignment_string, fontsize = 6)
                plt.text(x_text_coord, y_text_coord - 0.25*hBinY, r"$\pm$ " + error_string, fontsize = 6)

        plt.colorbar()
        self.pdf.savefig()
        plt.close()
