#!/usr/local/bin/python3

# This functions parses the XML files of a sTGC-QC
# repository of QA/QC spreadsheets to get the parenting
# of quadruplets to strip cathode boards
#
# Loop through one repository and outputs the parenting
# Function must be run multiple times will save the results to
# the parenting.sql file.

import argparse
import os
import xml.etree.ElementTree as et
import sqlite3 as sql
import Tools as my
from DBManager import ParentingDBManager

# Arguments parsers
def ParseInput():
    parser = argparse.ArgumentParser(description='Get parenting of quadruplets to strip cathode boards')
    parser.add_argument('repo', action='store', help='Path to repository')
    return parser.parse_args()
    


# Get the list of all XML files in a QA/QC repository
def getListXML(dir):
    listXML = []
    for root, dirs, files in os.walk(dir):
        for f in files:
            if f.lower().endswith(".xml"):
                name = os.path.join(root[root.find(dir) + len(dir):], f)
                if "master" not in name.lower():
                    listXML.append(name)

    listXML = [x[1:] if os.path.isabs(x) else x for x in listXML]
    return listXML

# Return empty children table if file not exists of
# if problem when parsing XML
def GetChildrenXML(xml):
    try:
        tree = et.parse(xml)
        root = tree.getroot()
        parentingNode = root.findall("./*/parenting")[0]
        children = []
        for row in parentingNode.findall('./value'):
            rowSplit = row.text.split(',')
            if len(rowSplit)==4:
                if rowSplit[3]=='Child':
                    eqGroup = rowSplit[0]
                    SN = rowSplit[1]
                    children.append( (eqGroup,SN) )

        return children
    except:
        return []

def FilterEqGroup(childTable, eqGroup):
    filteredTable = []
    for thisEqGroup, SN in childTable:
        if thisEqGroup == eqGroup:
            filteredTable.append(SN)

    return filteredTable



    

# Main function
if __name__ == "__main__":
    print("**** Get parenting ****")
    args = ParseInput()
    pathToRepo = args.repo
    if not my.IsValidDir(pathToRepo):
        raise Exception('The specified QA/QC repository is invalid')


    # Initialize DB manager
    db = ParentingDBManager()
    db.Open()
    
       
    # Get all XML files in repo
    listXML = getListXML(pathToRepo)

    # Get list of quadruplets (with SN and P/C)
    listQuads = []  # list of typle: (path, SN, logic P or C)
    for xml in listXML:
        splitPath = xml.split('/')
        if len(splitPath)==2 and splitPath[0]=='quadruplets':
            quadSplit = splitPath[1].split('.')
            if len(quadSplit)==4:
                quadType = quadSplit[0]
                if not my.IsValidQuadType(quadType):
                    raise Exception('Invalid quadruplet type:'+quadType)
                
                logic = quadSplit[1]
                if not (logic=='C' or logic=='P'):
                    raise Exception('Invalid quadruplet logic: '+logic)

                if not quadSplit[2].isdigit():
                    raise Exception('Invalid quadruplet SN: '+quadSplit[2])
                
                quadType = quadType + logic
                quadSN = int(quadSplit[2])
                listQuads.append(  (xml, int(quadSN), quadType) )
        
    insertCount=0
    for quad in listQuads:
        
        fullPathToXML = os.path.join(pathToRepo, quad[0])
        print("Processing quad XML: ", fullPathToXML)

        # Get doublets (and make dict of partID to SN)
        doublets = FilterEqGroup(GetChildrenXML(fullPathToXML), 'Doublets')                

        # Get gas volumes (and make dict of layer to SN)
        gasVolumes = []
        for doubletSN in doublets:
            path = os.path.join(pathToRepo, 'doublets', doubletSN+'.xml')
            gv = FilterEqGroup(GetChildrenXML(path), 'Gas volumes')
            gasVolumes.extend(gv)

        gvDict = dict()
        for sn in gasVolumes:
            snSplit = sn.split('.')
            if len(snSplit)==3 and len(snSplit[1])==2:
                layer = int(snSplit[1][1])
            else:
                raise Exception('Corrupted gas volume SN: '+sn)

            gvDict[layer] = sn


        # Get half strips
        hsDict = dict() # Half-strip dict
        for layer, gvSN in gvDict.items():
            path = os.path.join(pathToRepo, 'gas_volumes', gvSN+'.xml')
            hs = FilterEqGroup(GetChildrenXML(path), 'Half-strips')
            if len(hs) == 1:
                hsDict[layer] = hs[0]

        # Get cathode boards (finally)
        cbDict = dict() # cathode boards dict
        for layer, sn in hsDict.items():
            path = os.path.join(pathToRepo, 'half_strips', sn+'.xml')
            cb = FilterEqGroup(GetChildrenXML(path), 'Strip plates')
            if len(cb) == 1:
                cbDict[layer]=cb[0]

        # Export to DB
        quadSN = quad[1]
        quadType = quad[2]

        for layer in range(1,5):
            if layer in cbDict:
                sn=cbDict[layer]
                snSplit = sn.split('.')
                if len(snSplit)!=3:
                    raise Exception('Invalid sn for cathode board: '+sn)

                finalSNStr = snSplit[2]
                if finalSNStr[0]=='P' or finalSNStr[0]=='C':
                    finalSNStr = finalSNStr[-3:]
                
                finalSN = int(finalSNStr)
            else:
                finalSN = -1

            if db.AddBoard(quadType, quadSN, layer, finalSN):
                 insertCount =  insertCount+1

    # Finalize
    print('Boards inserted to DB:',  insertCount)
    db.Close()
