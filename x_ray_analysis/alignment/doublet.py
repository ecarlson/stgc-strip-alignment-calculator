# The class for the doublet, which will contain two board objects

from NomGeo import NomDoubGeo
from NomGeo import NomBoardGeo
from board import board

class doublet(object):
    def __init__(self, quad_type, doub_geo_nom, qc):

        # Copy data members
        self.quad_type = quad_type
        self.geo_nom=doub_geo_nom
        self.qc=qc

        # Consistency checks
        if list(qc.keys())!=[1,2]:
            raise Exception('QC results from 2 layers [1,2] are required for the doublet!')
            
        # Create the boards that make up the doublet
        self.layers = dict()
        self.layers[1] = board(quad_type, NomBoardGeo(doub_geo_nom, 1), qc[1], False)
        self.layers[2] = board(quad_type, NomBoardGeo(doub_geo_nom, 2), qc[2], True)
