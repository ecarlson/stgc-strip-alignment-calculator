import numpy as np
import numbers
import decimal
import scipy
import math
import sys
from matplotlib.image import NonUniformImage
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt

from NomGeo import NomBoardGeo
from NomGeo import NomDoubGeo

# The board class, which will have a collection of strips on it.
# The actual offset is computed here
class board(object):
    def __init__(self, quad_type, board_geo_nom, qc, is_first_half):
        # Copy data members
        self.quad_type = quad_type
        self.geo_nom = board_geo_nom
        self.qc=qc
        self.is_first_half = is_first_half
                
        scale_corr = -(self.qc['scale']/self.geo_nom.h)*(self.geo_nom.f - self.geo_nom.b)
        angle_corr = -self.geo_nom.a * math.tan(self.qc['anglerad'])
        nonpar_corr = self.geo_nom.a * math.tan( (self.geo_nom.f-self.geo_nom.b)*(1.+(self.qc['scale']/self.geo_nom.h))/(self.qc['scale']+self.geo_nom.h) * math.atan(self.qc['nonpar']/(2.*self.geo_nom.alpha))  )
        if self.quad_type in ['QL1','QL2','QL3']:
            nonpar_corr *=-1
        if self.is_first_half:
            nonpar_corr *=-1
            
        self.qc['offset_actual'] = self.qc['offset'] + scale_corr + angle_corr + nonpar_corr
        
        # PROBABLY USELESS VARIABLES which were used in previous versions of the code
        # Get top/bottom strip numbers
        # Will need to check if work with all modules types other than QS3

#        self.middle_strip_number = self.geo_nom.middleIndex
#        if is_first_half:
#            self.bottom_strip_number = 6
#            self.top_strip_number = self.geo_nom.n_strips - self.bottom_strip_number
#        else:
#            self.bottom_strip_number = 5
#            self.top_strip_number = self.geo_nom.n_strips - self.bottom_strip_number - 2
