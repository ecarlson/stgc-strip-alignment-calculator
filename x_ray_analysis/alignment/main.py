#!/usr/local/bin/python3

##########################################################
# Modified version of Evan's calculator with interface   #
# to a SQLite DB.                                        #
#                                                        #
# Author: Benoit Lefebvre (benoit.lefebvre@cern.ch)      #
# Based on: Evan Carlson (evan.michael.carlson@cern.ch)  #
# Date of creation: 2019-04-28                           #
##########################################################

# Import statements
import argparse
import csv
from NomGeo import NomQuadGeo
import DBManager
from DBManager import QCDBManager, ParentingDBManager
import math
from quadruplet import quadruplet
from doublet import doublet
from board import board
from x_ray_point import x_ray_point, quad_x_ray_results
import Tools as my
from matplotlib.image import NonUniformImage
import matplotlib.pyplot as plt
import pandas as pd
from openpyxl import load_workbook
from matplotlib.backends.backend_pdf import PdfPages
from MisalignmentPlotter import MisalignmentPlotter


# Arguments parsers
def ParseInput():
    parser = argparse.ArgumentParser(description='sTGC misalignment calculator')
    parser.add_argument('quad_type',  action='store', help='Quadruplet type. Possible choices: Q{S,L}{1,2,3}{P,C}')
    parser.add_argument('SN',  action='store', help='Serial\# of module.')
    parser.add_argument('--output', action='store', help='PDF output. default=o.pdf')
    parser.add_argument('--coordinates', action='store',help='Coordinate system. Possible choices: Wedge or Pin, default=Pin')
    return parser.parse_args()

# Main function
if __name__ == "__main__":

    # Start program / parse arguments
    print('**** sTGC misalignment calculator ****')
    args = ParseInput()
    quadType = args.quad_type
    if not my.IsValidSubQuadType(quadType):
        raise Exception("Invalid quadruplet type!")

    subQuadType = quadType[0:3]
    quadSN = args.SN

    if args.coordinates:
        coords = args.coordinates
        if not my.IsValidCoords(coords):
            raise Exception("Invalid coordinate system!")
    else:
        coords = 'Pin'
    print("The coordinate system used for this run is the " + coords+ " coordinate system.")

        
    if args.output:
        outPDF = args.output
    else:
        outPDF = quadType+'_SN'+str(quadSN)+'.pdf'
        
    print('Quadruplet type: ', quadType)
    print("Serial number: ", quadSN)
    print('PDF output: ', outPDF)



    # Get nominal geo
    nomQuadGeo = NomQuadGeo(subQuadType)
    
    # Get parenting
    dbParenting = ParentingDBManager()
    dbParenting.Open()
    boardsSN = dict()
    for layer in range(1,5):
        sn = dbParenting.GetBoardSN(quadType, quadSN, layer)
        if sn<0:
            raise Exception('Parenting for '+quadType+' SN '+quadSN+' not found or not complete.')
        else:
            boardsSN[layer]=sn
    
    print('Strip cathode boards parenting:', boardsSN)
    dbParenting.Close()
    
    # Get QA/QC measurements
    dbQC = QCDBManager()
    dbQC.Open()
    boardsQC = dict()
    for layer in range(1,5):
        if layer==1 or layer==3:
            boardType = '13'
        elif layer==2 or layer==4:
            boardType = '24'
        else:
            boardType = ''

        thisqc = dbQC.GetQCMeas(subQuadType, boardType, boardsSN[layer])
        if not DBManager.ValidateQCValues(thisqc):
            raise Exception('Invalid QC measurements for board '+subQuadType+' '+boardType+' '+boardsSN[layer])
        else:
            boardsQC[layer] = thisqc        
        
    dbQC.Close()
    print('QC measurements:', boardsQC)
    
    # Construct detector objects
    print('Initializing quadruplet object...')
    quad = quadruplet(subQuadType, nomQuadGeo, boardsQC, coords)
    print(subQuadType)

    print("All measurements are presented in millimeters.")
    print(str(quad.geo_nom.__str__()))
    #Compute misalignments
    print('Computation of misalignments')
    with PdfPages(outPDF) as pdf:
        alignModule = MisalignmentPlotter(quad, outPDF)
        alignModule.pdf = pdf
        alignModule.ComputeMisalignmentsAllLayersCombinations(nBinsX=12, nBinsY=12, nPoints=21)
        #alignModule.PrintAllFigure()
    
    
    print('**** Goodbye! ****')
