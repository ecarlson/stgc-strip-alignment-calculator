# Defines a quadruplet
# A quadruplet is made up of 2 doublets.
# The nominal geometry and QC measurements are stored as well
#
# quad_type: QS1, QS2, QS3, QL1, QL2, QL3
# geo_nom: nominal geometry
# qc: QC measurements->offset, angle, nonpar, scale
from NomGeo import NomQuadGeo
from NomGeo import NomDoubGeo
from doublet import doublet
import math

# Check if layers are valid, must be between 1 and 4
# Also checks for duplicates
def AreValidUniqueLayers(layers):
    for l in layers:
        if l not in [1,2,3,4]:
            return False
    return len(layers) == len(set(layers))


class quadruplet(object):
    def __init__(self, quad_type, quad_geo_nom, qc, coord_system):

        # Copy data members
        self.quad_type = quad_type
        self.geo_nom = quad_geo_nom
        self.qc = qc
        self.coord_system = coord_system

        

        # If the user wants to work in the wedge coordinate system, change the min and max values of the plotting accordingly. The yMin and yMax values are flipped because of the flipped y-axis.
        if self.coord_system == 'Wedge':
            self.geo_nom.yMin = self.geo_nom.wedge_y - self.geo_nom.yMin
            self.geo_nom.yMax = self.geo_nom.wedge_y - self.geo_nom.yMax
            temp_min = self.geo_nom.yMin
            self.geo_nom.yMin = self.geo_nom.yMax
            self.geo_nom.yMax = temp_min

            self.geo_nom.xMin = self.geo_nom.xMin - self.geo_nom.a
            self.geo_nom.xMax = self.geo_nom.xMax - self.geo_nom.a


        # Consistency checks
        if list(qc.keys())!=[1,2,3,4]:
            raise Exception('QC results from 4 layers [1,2,3,4] are required for the quadruplet!')
        
        # Create the doublets that make up the quadruplet
        self.doublets = dict()
        self.doublets['12'] = doublet(quad_type, NomDoubGeo(quad_geo_nom, [1,2]), {1:qc[1], 2:qc[2]})
        self.doublets['34'] = doublet(quad_type, NomDoubGeo(quad_geo_nom, [3,4]), {1:qc[3], 2:qc[4]}) 


    def GetBoard(self, layer):
        if layer==1:
            return self.doublets['12'].layers[1]
        elif layer==2:
            return self.doublets['12'].layers[2]
        elif layer==3:
            return self.doublets['34'].layers[1]
        elif layer==4:
            return self.doublets['34'].layers[2]
        else:
            raise Exception('Invalid layer.')

    # Checks whether a (x,y) space point is inside the trapezoid
    def InTrapezoid(self, x, y):
        if self.quad_type == 'QL3':
            raise Exception('This function does not support the QL3 quadruplet.')
        
        # Check is one of limits is NaN
        if math.nan in [self.geo_nom.yMin, self.geo_nom.yMax, self.geo_nom.xMin, self.geo_nom.xMax]:
            return False

        # Check if outside y-limits
        if y>self.geo_nom.yMax or y<self.geo_nom.yMin:
            return False

        sideSlope = math.tan(self.geo_nom.boardAngle*math.pi/180.0)
        H = self.geo_nom.yMax-self.geo_nom.yMin

        beginSmallBase = self.geo_nom.xMin+sideSlope*H
        endSmallBase = self.geo_nom.xMax-sideSlope*H

        # Check if within the small base edges.
        if x>beginSmallBase and x<endSmallBase:
            return True

        # Check if under the small base
        if self.coord_system == 'Pin':                    
            if x<beginSmallBase:
                return y < ((x-self.geo_nom.xMin)/sideSlope+self.geo_nom.yMin)
            elif x>endSmallBase:
                return y < ( self.geo_nom.yMax - (x-endSmallBase)/sideSlope )
            else:
                return False

        if self.coord_system == 'Wedge':
            if (x > (self.geo_nom.xMin + (self.geo_nom.yMax - y) * sideSlope)) and (x < (self.geo_nom.xMax - (self.geo_nom.yMax - y) * sideSlope)):
                return True
            else:
                return False


        
    # Computes misalignments of l3 and l4 w.r.t. l1 and l2 at point (x,y)
    # Returns the misalignment relative to fitted track and error as a tuple
    def track_misalignments(self, x, y, refLayers, alignLayers):
        if (not AreValidUniqueLayers(refLayers+alignLayers)) or len(refLayers)!=2 or len(alignLayers)!=2:
            raise Exception('Layers used as input are invalid.')

        # The 2 last entries of yTrack correspond to the align layers
        yTrack, yTrack_unc = self.track_expectation(x, y,  refLayers, alignLayers)
        yAlignPrime       = [self.continuous_transform(x, y, l) for l in alignLayers]
        yAlignPrimeUnc    = [self.continuous_uncertainty(x, y, l) for l in alignLayers]
        yAlignResidual    = [yAlignPrime[lIdx]-yTrack[2:][lIdx] for lIdx in [0,1]]
        yAlignResidualUnc = [math.sqrt(math.pow(yAlignPrimeUnc[lIdx],2) + math.pow(yTrack_unc[2:][lIdx],2)) for lIdx in [0,1]]

        return yAlignResidual, yAlignResidualUnc


    # Computes the shifted position of refLayers.
    # Get track position on alignLayers
    # Return 2 arrays 4 space points: values and errors
    def track_expectation(self, x, y, refLayers, alignLayers):
        if not AreValidUniqueLayers(refLayers+alignLayers) or len(refLayers)!=2 or len(alignLayers)!=2:
            raise Exception('Layers used as input are invalid.')

        y_prime=[]
        y_prime_unc=[]
        y_prime.append(self.continuous_transform(x, y, refLayers[0]))
        y_prime.append(self.continuous_transform(x, y, refLayers[1]))
        y_prime_unc.append(self.continuous_uncertainty(x, y, refLayers[0]))
        y_prime_unc.append(self.continuous_uncertainty(x, y, refLayers[1]))

        allLayers = refLayers+alignLayers
        z = [self.geo_nom.z[l] for l in allLayers]
        slope = (y_prime[1]-y_prime[0]) / (z[1]-z[0])
        slope_unc = math.fabs(1/(z[1]-z[0])) * math.sqrt(math.pow(y_prime_unc[0],2)+math.pow(y_prime_unc[1],2))
        offset = y_prime[0]-slope*z[0]
        offset_unc = math.sqrt( math.pow(y_prime_unc[0],2) + math.pow(slope_unc*z[0],2) )

        y_prime.append(slope*z[2]+offset)
        y_prime.append(slope*z[3]+offset)

        y_prime_unc.append( math.sqrt( math.pow(slope_unc*z[2], 2) + math.pow(offset_unc, 2) ) )
        y_prime_unc.append( math.sqrt( math.pow(slope_unc*z[3], 2) + math.pow(offset_unc, 2) ) )

        return y_prime, y_prime_unc

    # Returns the shifted value of y according to the board transform
    def continuous_transform(self, x, y, layerIdx):
        board = self.GetBoard(layerIdx)
        g = board.geo_nom
        qc = board.qc

        if self.coord_system == 'Pin':
            dScale = (qc['scale'] / g.h)*(y - g.b)
            tanAngle = math.tan(qc['anglerad'])
            da = -(x - g.a)*tanAngle
            dOffsetActual = qc['offset_actual']
            dNonPar = (x - g.a) * math.tan( (y-g.b) * (1.+(qc['scale']/g.h)) / (qc['scale']+g.h) * math.atan(qc['nonpar']/(2*g.alpha)))

            if board.quad_type in ['QL1', 'QL2', 'QL3']:
                dNonPar *= -1
            if board.is_first_half:
                dNonPar *= -1

            y_prime = y + dOffsetActual + dScale + dNonPar + da

            return y_prime
        elif self.coord_system == 'Wedge':
            dScale = (qc['scale'] / g.h)*(y + g.b - self.geo_nom.wedge_y)
            tanAngle = math.tan(qc['anglerad'])
            da = x * tanAngle
            dOffsetActual = -1 * qc['offset_actual']
            dNonPar = x * ((y + g.b - self.geo_nom.wedge_y)/g.h) * (qc['nonpar']/(2*g.alpha))

            if board.quad_type in ['QL1', 'QL2', 'QL3']:
                dNonPar *= -1
            if board.is_first_half:
                dNonPar *= -1

            y_prime = y + dOffsetActual + dScale + dNonPar + da

            return y_prime 
        else:
            raise Exception("Invalid coordinate system!")           


    # Error estimation on the y coordinate of the shifted point on layer_layerIdx
    def continuous_uncertainty(self, x, y, layerIdx):
        # Get board and board data
        board = self.GetBoard(layerIdx)
        g = board.geo_nom
        qc = board.qc

        # Error estimation of CMM measurements
        scale_unc = 0.030
        nonpar_unc = 0.030
        offset_unc = 0.030
        angle_unc = 0.0000175

        if self.coord_system == 'Pin':
            scale_term = ((y-g.b)/g.h - (g.f-g.b)/g.h  )*scale_unc
            offset_term = offset_unc
            angle_term = ((x - g.a) * math.pow(math.cos(qc['angle']*math.pi/180.), -2) + g.a * math.pow(math.cos(qc['angle']*math.pi/180.), -2)) * angle_unc

            nonpar_term = ((1 / (2 * g.alpha * g.h)) * (1 / (1 + math.pow(qc['nonpar'] / (2 * g.alpha),2))) * ((x - g.a) * (y - g.b) * math.pow(math.cos((y - g.b) / g.h * math.atan(qc['nonpar'] / (2 * g.alpha))),-2) +g.a * (g.f - g.b) * math.pow(math.cos((g.f - g.b) / g.h * math.atan(qc['nonpar'] / (2 * g.alpha))),-2))) * nonpar_unc
            
            net_uncertainty = math.sqrt(math.pow(scale_term,2) + math.pow(offset_term,2) + math.pow(angle_term,2) + math.pow(nonpar_term,2))

            return net_uncertainty
        elif self.coord_system == 'Wedge':
            scale_term = (y + g.f - self.geo_nom.wedge_y) / g.h * scale_unc
            offset_term = offset_unc
            angle_term = (x + g.a) * math.pow(math.cos(qc['angle'] * math.pi/180.0),-2) * angle_unc

            nonpar_term = (x * (y + g.b - self.geo_nom.wedge_y) / (2.0 * g.h * g.alpha) - g.a * (g.f - g.b) / (2.0 * g.alpha * g.h)) * nonpar_unc

            net_uncertainty = math.sqrt(math.pow(scale_term,2) + math.pow(offset_term,2) + math.pow(angle_term,2) + math.pow(nonpar_term,2)) 

            return net_uncertainty
        else:
            raise Exception("Invalid coordinate system!")
