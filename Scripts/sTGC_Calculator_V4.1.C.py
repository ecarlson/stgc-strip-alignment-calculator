# This script will implement the full structure of the doublets
# by creating doublet objects, each with two board objects,
# each of which will have a number
# of strip objects equal to the number of strips on the board.
# The instance variables of the board should be the measurements
# of the noncomformities, the type of the board, and the
# number of strips. 

# This transform and code were authored by Evan M. Carlson,
# University of Victoria / TRIUMF.
# Version 4.1.C - November 26th, 2018

# Version 4.1.C is a version that compares to the Carleton pictures. 
# It automatically inputs the strip index and does the misalignments
# for point 1, 2, 3, and 4 in one run of the program

#####################################################################
################ VERY IMPORTANT NOTES ON ORIENTATION ################
# All of this program is written with the orientation of the long   
# edge of the doublet being on the bottom. That is where the v      
# notch pin is located, and that is the center of the coordinate    
# system. 



import numpy as np
import numbers
import decimal
import scipy
import math
import sys
import matplotlib.pyplot as plt

# Here's the stuff that's necessary for it to work with google sheets.
import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('stgc_client.json',scope)
client = gspread.authorize(creds)



print("This program assembles the strip geometry of an sTGC doublet based on the provided measurements.")

# Get the type of the board from the user.

board_type = input("What type of board are we working with? Enter 1 for QS1, 2 for QS2, 3 for QS3, 4 for QL1, 5 for QL2, and 6 for QL3. ")


while(board_type != 1 and board_type != 2 and board_type != 3 and board_type != 4 and board_type != 5 and board_type != 6):
        board_type = input("Please input a valid board type. ")



# Let's define the strip class of objects, which will just be lines for now.

class strip:

        # Initializer - Instance Attributes
        def __init__(self, y_coordinate, strip_number,scale,offset,nonpar,angle,offset_actual,n_strips,h,alpha,a,b,f,x_0,board_angle,is_first_half):
                # Each strip will have a strip number and a nominal y coordinate.
                self.strip_number = strip_number
                self.y_coordinate = y_coordinate

                # Here are the nominal values for the strip and board.
                self.n_strips = n_strips
                self.h = h
                self.alpha = alpha
                self.a = a
                self.b = b
                self.f = f
                self.x_0 = x_0

                # Each strip will inherit the measurements from the board.
                self.scale = scale
                self.offset = offset
                self.nonpar = nonpar
                self.angle = angle
                self.offset_act = offset_actual
                self.is_first_half = is_first_half
                self.board_angle = board_angle

                # This creates the set of points that will make up the line. They all
                # nominally have the same y coordinate before being transformed.
                # NOTE: THE LEFT ENDPOINTS ARE ON THE ALIGNMENT SIDE OF THE BOARD. THIS IS VERY IMPORTANT
                # SO THE RIGHT ENDPOINTS ARE ON THE NOT ALIGNMENT SIDE. THIS IS  VERY IMPORTANT
                self.left_endpoint = np.array([x_0 + (self.strip_number - 1) * strip_pitch * math.tan(self.board_angle * math.pi / 180.0),y_coordinate])
                self.left_midpoint = np.array([a - alpha, y_coordinate])
                self.centerpoint = np.array([a, y_coordinate])
                self.right_midpoint = np.array([a + alpha, y_coordinate])
                self.right_endpoint = np.array([2*a - x_0 -  (self.strip_number - 1) * strip_pitch * math.tan(self.board_angle * math.pi / 180.0), y_coordinate])
                self.points = np.array([self.left_endpoint, self.left_midpoint, self.centerpoint, self.right_midpoint, self.right_endpoint])
                
        # The function that transforms the points on the line. 
        def transform(self,x,y):
                if self.is_first_half == 0:
                    y_prime = y + self.offset_act + (self.scale / self.h)*(y - self.b) + (x - self.a) * math.tan((y - self.b) * (1.0+(self.scale/self.h)) / (self.scale+self.h) * math.atan(self.nonpar/(2.0*self.alpha))) - (x - self.a)*math.tan(self.angle*math.pi/180.0)
                else:
                    y_prime = y + self.offset_act + (self.scale / self.h)*(y - self.b) - (x - self.a) * math.tan((y - self.b) * (1.0+(self.scale/self.h)) / (self.scale+self.h) * math.atan(self.nonpar/(2.0*self.alpha))) - (x - self.a)*math.tan(self.angle*math.pi/180.0)
                return x,y_prime



# The board class, which will have a number of strips on it.
class board:

        # Initializer - Instance Attributes
        def __init__(self, board_type_arg, scale_arg, offset_arg, nonpar_arg, angle_arg, is_first_half,n_strips,h_arg, alpha_arg, a_arg , b_arg , f_arg, middle_strip_index,x_0,board_angle):

                # Give the board the measurements and the board type.
                self.board_type = board_type_arg
                self.scale = scale_arg
                self.offset = offset_arg
                self.nonpar = nonpar_arg
                self.angle = angle_arg

                # Here are the nominal values for the board.
                self.n_strips = n_strips
                self.h = h_arg
                self.alpha = alpha_arg
                self.a = a_arg
                self.b = b_arg
                self.f = f_arg
                self.x_0 = x_0
                self.board_angle = board_angle

                # The relevant strip indices.
                self.bottom_strip_number = 5 +  is_first_half
                self.middle_strip_number = middle_strip_index

                # These indices work for QS3 boards, but I'm not sure if they hold for boards with an even number of strips.
                if is_first_half == 0:
                    self.top_strip_number = self.n_strips - self.bottom_strip_number - 2
                elif is_first_half == 1:
                    self.top_strip_number = self.n_strips - self.bottom_strip_number

                # Calculate the actual offset.
                if is_first_half == 0:
                    self.offset_actual = self.offset - (self.scale/self.h)*(self.f - self.b) - self.a * math.tan(self.angle * math.pi / 180.) + self.a * math.tan((self.f-self.b)*(1.+(self.scale/self.h))/(self.scale+self.h)* math.atan(self.nonpar/(2.*self.alpha)))
                else:
                    self.offset_actual = self.offset - (self.scale/self.h)*(self.f - self.b) - self.a * math.tan(self.angle * math.pi / 180.) - self.a * math.tan((self.f-self.b)*(1.+(self.scale/self.h))/(self.scale+self.h)* math.atan(self.nonpar/(2.*self.alpha)))

                # 1 means the first strip is a half strip, 0 is a full strip
                self.is_first_half = bool(is_first_half)
                self.strips = np.empty([n_strips], dtype = object)

                # This block will be used to differentiate between the 13 and 24 boards.
                if is_first_half == True:
                    for i in range(0,n_strips):
                        self.strips[i] = strip(self.b + (i-6) * strip_pitch, i + 1, self.scale, self.offset, self.nonpar, self.angle,self.offset_actual,self.n_strips,self.h,self.alpha,self.a,self.b,self.f,self.x_0,self.board_angle,self.is_first_half)
                elif is_first_half == False:
                    for i in range(0,n_strips):
                        self.strips[i] = strip(self.b + (i-5) * strip_pitch, i + 1, self.scale, self.offset, self.nonpar, self.angle,self.offset_actual,self.n_strips,self.h,self.alpha,self.a,self.b,self.f,self.x_0,self.board_angle,self.is_first_half)

                # Make an array of the endpoints which will be used for comparing to measurements.
                self.non_alignment_endpoints = np.empty([2,n_strips])
                self.alignment_endpoints = np.empty([2,n_strips])

                for i in range(0,n_strips):
                    # This is the x-coordinate. Note that calling the opposite endpoint is because of the flipped coordinate system
                    # FOR BOARDS, THE LEFT ENDPOINT IS NOT ALIGNMENT SIDE AND THE RIGHT ENDPOINT IS ALIGNMENT SIDE.
                    # THIS MATCHES THE PHYSICAL SETUP THAT WE HAVE. IF THE BOARD IS ORIENTED WITH THE LONG EDGE ON THE BOTTOM,
                    # THE ALIGNMENT FEATURES ARE ON THE RIGHT. SO THE BOARD RIGHT ENDPOINT IS ALIGNMENT SIDE
                    self.non_alignment_endpoints[0][i] = self.strips[i].right_endpoint[0]
                    self.alignment_endpoints[0][i] = self.strips[i].left_endpoint[0]
                    # This is the y-coordinate.
                    self.non_alignment_endpoints[1][i] = self.strips[i].right_endpoint[1]
                    self.alignment_endpoints[1][i] = self.strips[i].left_endpoint[1]



        # Define a method to look at a particular strip.
        def get_strip(self,number):
            return self.strips[number]
        

        # A method to print the strips. Should yield a trapezoidal plot.
        def display_geometry(self):
            x_values = np.empty([5])
            y_values = np.empty([5])
            
            for i in range(0,n_strips):
                for j in range(0,5):
                    x_values[j] = self.get_strip(i).points[j][0]
                    y_values[j] = self.get_strip(i).points[j][1]

                plt.plot(x_values,y_values)

            plt.show()

        # A method to display only the relevant strips.
        def display_important_strips(self):
            x_values = np.empty([5])
            y_values = np.empty([5])

            important_strips = np.array([self.strips[self.bottom_strip_number],self.strips[self.middle_strip_number],self.strips[self.top_strip_number]])

            for item in important_strips:
                for j in range(0,5):
                    x_values[j] = item.points[j][0]
                    y_values[j] = item.points[j][1]
                plt.plot(x_values,y_values)

            plt.show()

         # A method to transform the strips.
        def strip_transform(self):

            # Loop through the strips and transform them one by one.
            for i in range(0,n_strips):
                index = 0
                while index < len(self.strips[i].points):
                    self.strips[i].points[index][0],self.strips[i].points[index][1] = self.strips[i].transform(self.strips[i].points[index][0],self.strips[i].points[index][1])
                    index += 1

            for i in range(0,n_strips):
                # This is the x-coordinate. Note that calling the right_endpoint is because of the flipped coordinate system
                self.non_alignment_endpoints[0][i] = self.strips[i].right_endpoint[0]
                self.alignment_endpoints[0][i] = self.strips[i].left_endpoint[0]
                # This is the y-coordinate.
                self.non_alignment_endpoints[1][i] = self.strips[i].right_endpoint[1]
                self.alignment_endpoints[1][i] = self.strips[i].left_endpoint[1]            


        # Here's a method to obtain the measurements.
        def get_measurements(self):
            # Calculate d1 and d2.
            # This if statement compensates for the fact that the 24 board is flipped over.
            if self.is_first_half == False:
                d_2_measurement = self.strips[self.top_strip_number].points[1][1] - self.strips[self.bottom_strip_number].points[1][1]
                d_1_measurement = self.strips[self.top_strip_number].points[3][1] - self.strips[self.bottom_strip_number].points[3][1]
            elif self.is_first_half == True:
                d_1_measurement = self.strips[self.top_strip_number].points[1][1] - self.strips[self.bottom_strip_number].points[1][1]
                d_2_measurement = self.strips[self.top_strip_number].points[3][1] - self.strips[self.bottom_strip_number].points[3][1]

            # Calculate all of the measurements for the board.
            scale_measurement = (d_1_measurement + d_2_measurement) / 2.0 - h
            nonpar_measurement = d_1_measurement - d_2_measurement
            angle_measurement = math.atan((self.strips[self.bottom_strip_number].points[0][1] - self.strips[self.bottom_strip_number].points[4][1]) / (self.strips[self.bottom_strip_number].points[4][0] - self.strips[self.bottom_strip_number].points[0][0])) * 180.0 / math.pi
            
            # Because the strips are not all the same length, we need to extrapolate the line of the middle strip
            # in order to get the offset measurement. To do this, we extend the line out from the edge
            # until it crosses our y axis directly above the pin.
            slope = (self.strips[self.middle_strip_number].points[2][1] - self.strips[self.middle_strip_number].points[1][1]) / (self.strips[self.middle_strip_number].points[2][0] - self.strips[self.middle_strip_number].points[1][0])
            offset_measurement = self.strips[self.middle_strip_number].points[1][1] - (self.a - self.alpha) * slope - self.f

            return scale_measurement, offset_measurement, nonpar_measurement, angle_measurement, d_1_measurement, d_2_measurement

# The class for the doublet, which will contain two board objects
class doublet:

        def __init__(self,board_type,board_angle,h_nom,a_nom,n_strips,alpha_13_nom,b_13_nom,f_13_nom,mid_index_13,x_0_13,alpha_24_nom,b_24_nom,f_24_nom,mid_index_24,x_0_24,scale_13,scale_24,nonpar_13,nonpar_24,angle_13,angle_24,offset_13,offset_24):

                # Give the doublet all of it's values.
                self.board_type = board_type
                self.board_angle = board_angle

                self.h_nom = h_nom
                self.a_nom = a_nom
                self.n_strips = n_strips

                self.alpha_13_nom = alpha_13_nom
                self.b_13_nom = b_13_nom
                self.f_13_nom = f_13_nom
                self.mid_index_13 = mid_index_13
                self.x_0_13 = x_0_13
                
                self.alpha_24_nom = alpha_24_nom
                self.b_24_nom = b_24_nom
                self.f_24_nom = f_24_nom
                self.mid_index_24 = mid_index_24
                self.x_0_24 = x_0_24

                self.scale_13 = scale_13
                self.nonpar_13 = nonpar_13
                self.angle_13 = angle_13
                self.offset_13 = offset_13

                self.scale_24 = scale_24
                self.nonpar_24 = nonpar_24
                self.angle_24 = angle_24
                self.offset_24 = offset_24

                # Create the boards that make up the doublet.
                self.layer_1 = board(self.board_type,self.scale_13,self.offset_13,self.nonpar_13,self.angle_13,0,self.n_strips,self.h_nom,self.alpha_13_nom,self.a_nom,self.b_13_nom,self.f_13_nom,self.mid_index_13,self.x_0_13,self.board_angle)
                self.layer_2 = board(self.board_type,self.scale_24,self.offset_24,self.nonpar_24,self.angle_24,1,self.n_strips,self.h_nom,self.alpha_24_nom,self.a_nom,self.b_24_nom,self.f_24_nom,self.mid_index_24,self.x_0_24,self.board_angle)

        # This function will compare the positions of the midpoint of adjacent strips in the two layers.
        # This function works for the nonalignment side.
        # They should be offset by approximately 1600 microns.
        def compare_strip_layers_non_alignment_side(self,strip_number):
            # Drop the strip number by 1, because I doubt they are using indexing from zero.
            index = strip_number - 1

            # This calls the right_endpoint of a strip object, which is thus the non_alignment side
            x_1 = self.layer_1.strips[index].points[4][0]
            x_2 = self.layer_2.strips[index].points[4][0]

            y_1 = self.layer_1.strips[index].points[4][1]
            y_2 = self.layer_2.strips[index].points[4][1]            

            distance = math.sqrt(math.pow((x_1 - x_2),2) + math.pow((y_1-y_2),2))
            nominal_distance = 1600.0 / math.cos(self.board_angle * math.pi / 180.0)
            difference = distance - nominal_distance
            # The negative one account for the flipped perspective of the camera taking the microscope pictures.
            # This number can be directly compared to the number on the xml files.
            return -1.0 * difference

        # This function will compare the positions of the midpoint of adjacent strips in the two layers.
        # This function works for the alignment side.
        # They should be offset by approximately 1600 microns.
        def compare_strip_layers_alignment_side(self,strip_number):
            # Drop the strip number by 1, because I doubt they are using indexing from zero.
            index = strip_number - 1

            # This calls the left_endpoint of a strip object, which is thus the alignment side
            x_1 = self.layer_1.strips[index].points[0][0]
            x_2 = self.layer_2.strips[index].points[0][0]

            y_1 = self.layer_1.strips[index].points[0][1]
            y_2 = self.layer_2.strips[index].points[0][1]            

            distance = math.sqrt(math.pow((x_1 - x_2),2) + math.pow((y_1-y_2),2))
            nominal_distance = 1600.0 / math.cos(self.board_angle * math.pi / 180.0)
            difference = distance - nominal_distance
            return difference

# Set the appropriate variables based on the board type.
if board_type == 1:
        n_strips = 406
        h = 1292800.0
        a = 371853.4
        board_angle = 8.5
        
        # NOTE: Need to fix the b values for this because they measured from the first full strip.
        b_13 = 16299.1
        f_13 = 646699.1
        alpha_13 = 163498
        middle_index_13 = 202
        x_0_13 = 2634.6

        b_24 = 17899.1
        f_24 = 648299.1
        alpha_24 = 163259.4
        middle_index_24 = 203
        x_0_24 = 2400.76

        sh = client.open('Copy of GS1 General Summary')

        sheet_13 = sh.get_worksheet(4)
        sheet_24 = sh.get_worksheet(5)

elif board_type == 2:
        n_strips = 365
        h = 1129600.0
        a = 362656.9
        board_angle = 8.5

        b_13 = 13299.1
        f_13 = 579699.1
        alpha_13 = 362656.9
        middle_index_13 = 182
        x_0_13 = 2186.34

        b_24 = 14899.1
        f_24 = 578099.1
        alpha_24 = 362418.2
        middle_index_24 = 182
        x_0_24 = 1952.46

        sh = client.open('Copy of GS2 General Summary')

        sheet_13 = sh.get_worksheet(2)
        sheet_24 = sh.get_worksheet(3)

elif board_type == 3:
        n_strips = 307
        h = 944000.0
        a = 703127.3
        board_angle = 8.5
        
        b_13 = 13299.1
        f_13 = 486899.1
        alpha_13 = 542566.4
        middle_index_13 = 153
        x_0_13 = 2186.34

        b_24 = 14899.1
        f_24 = 485299.1
        alpha_24 = 542327.7
        middle_index_24 = 153
        x_0_24 = 1952.46

        sh = client.open('Copy of GS3 General Summary')

        sheet_13 = sh.get_worksheet(4)
        sheet_24 = sh.get_worksheet(5)

elif board_type == 4:
        n_strips = 408
        h = 1267200.0
        a = 602161.1
        board_angle = 14.0
        
        b_13 = 16107.6
        f_13 = 649707.6
        alpha_13 = 264522.3
        middle_index_13 = 203
        x_0_13 = 2758.45

        b_24 = 17707.6
        f_24 = 651307.6
        alpha_24 = 264124.2
        middle_index_24 = 204
        x_0_24 = 2383.2

        sheet_name = 'Copy of GL1 General Summary'

elif board_type == 5:
        n_strips = 366
        h = 1132800.0
        a = 901754.3
        board_angle = 14.0
        
        b_13 = 13107.6
        f_13 = 579507.6
        alpha_13 = 598373.3
        middle_index_13 = 182
        x_0_13 = 2010.45

        b_24 = 14707.6
        f_24 = 581107.6
        alpha_24 = 597975.1
        middle_index_24 = 183
        x_0_24 = 1635.3

        sh = client.open('Copy of GL2 General Summary')

        sheet_13 = sh.get_worksheet(4)
        sheet_24 = sh.get_worksheet(5)

elif board_type == 6:
        n_strips = 353
        h = 1091200.0
        a = 1058035.5
        board_angle = 14.0
        
        b_13 = 13600.0
        f_13 = 560800.0
        alpha_13 = 899313.0
        middle_index_13 = 176
        x_0_13 = 2535.5

        b_24 = 15200.0
        f_24 = 559200.0
        alpha_24 = 898914.7
        middle_index_24 =  176
        x_0_24 = 2535.5

        sh = client.open('Copy of GL3 General Summary')

        sheet_13 = sh.get_worksheet(2)
        sheet_24 = sh.get_worksheet(3)

# This block of code will obtain the values of the 13 board
# measurements from the user. All of the values will be in microns
# The scale, offset, nonparallelism and angle are all entered
# as the deviations from the nominal.



print("This first set of measurements is for the 13 board.")
serial_13 = input("What is the serial number of layer 1 or 3 board? ")

scale_meas_13 = sheet_13.cell(8, serial_13 + 1).value

offset_meas_13 = sheet_13.cell(10,serial_13 + 1).value

nonpar_meas_13 = sheet_13.cell(9,serial_13 + 1).value

angle_meas_13 = sheet_13.cell(11,serial_13 + 1).value


# Do you have the d1 and d2 measurements? If so, you can input them here.
# The program will then compare them later after the transform
# include_d1_d2_13 = input("Do you have the D1 and D2 measurements? 1 for yes, 0 for no. ")

# while(include_d1_d2_13 != 1 and include_d1_d2_13 != 0):
#         include_d1_d2_13 = input("Please input a valid option. ")


# Take in the measurements if the user has them. 
# if include_d1_d2_13 == 1:
#     d1_meas_13 = input("What is the d1 value in microns? ")
#     d2_meas_13 = input("What is the d2 value in microns? ")


# Now we will take in the measurements for the 24 board.

print("This set of measurements is for the 24 board.")
serial_24 = input("What is the serial number of the layer 2 or 4 board? ")

scale_meas_24 = sheet_24.cell(8,serial_24 + 1).value

offset_meas_24 = sheet_24.cell(10,serial_24 + 1).value

nonpar_meas_24 = sheet_24.cell(9,serial_24 + 1).value

angle_meas_24 = sheet_24.cell(11,serial_24 + 1).value


# Do you have the d1 and d2 measurements? If so, you can input them here.
# The program will then compare them later after the transform
# include_d1_d2_24 = input("Do you have the D1 and D2 measurements? 1 for yes, 0 for no. ")

# while(include_d1_d2_24 != 1 and include_d1_d2_24 != 0):
#         include_d1_d2_24 = input("Please input a valid option. ")


# Take in the measurements if the user has them. 
# if include_d1_d2_24 == 1:
#     d1_meas_24 = input("What is the d1 value in microns? ")
#     d2_meas_24 = input("What is the d2 value in microns? ")

# The strip pitch in microns.
strip_pitch = 3200


# Cast the 13 data types.
# The values out of Google Sheets are unicode. You must convert them to a string and then a float to be able to use them.
scale_meas_13 = scale_meas_13.encode('utf-8')
offset_meas_13 = offset_meas_13.encode('utf-8')
nonpar_meas_13 = nonpar_meas_13.encode('utf-8')
angle_meas_13 = angle_meas_13.encode('utf-8')

scale_meas_13 = float(scale_meas_13)
offset_meas_13 = float(offset_meas_13)
nonpar_meas_13 = float(nonpar_meas_13)
angle_meas_13 = float(angle_meas_13)

# Convert to microns 
scale_meas_13 = 1000 * scale_meas_13
offset_meas_13 = 1000 * offset_meas_13
nonpar_meas_13 = 1000 * nonpar_meas_13

# Print out the values of the 13 measurements for the user.
print ("For the 13 board, the measurements are:")
print ("Scale = %3.1f " %scale_meas_13)
print ("Offset = %3.1f " %offset_meas_13)
print ("Nonparallelism = %3.1f " %nonpar_meas_13)
print ("Angle = %1.4f " %angle_meas_13)

# Cast the 24 data types.
# The values out of Google Sheets are unicode. You must convert them to a string and then a float to be able to use them.
scale_meas_24 = scale_meas_24.encode('utf-8')
offset_meas_24 = offset_meas_24.encode('utf-8')
nonpar_meas_24 = nonpar_meas_24.encode('utf-8')
angle_meas_24 = angle_meas_24.encode('utf-8')

scale_meas_24 = float(scale_meas_24)
offset_meas_24 = float(offset_meas_24)
nonpar_meas_24 = float(nonpar_meas_24)
angle_meas_24 = float(angle_meas_24)

# Convert to microns
scale_meas_24 = 1000 * scale_meas_24
offset_meas_24 = 1000 * offset_meas_24
nonpar_meas_24 = 1000 * nonpar_meas_24

# Print out the values of the 24 measurements for the user.
print ("For the 24 board, the measurements are:")
print ("Scale = %3.1f " %scale_meas_24)
print ("Offset = %3.1f " %offset_meas_24)
print ("Nonparallelism = %3.1f " %nonpar_meas_24)
print ("Angle = %1.4f " %angle_meas_24)


# Now we will define a 13 board with the parameters given by the user.
doublet_1 = doublet(board_type,board_angle,h,a,n_strips,alpha_13,b_13,f_13,middle_index_13,x_0_13,alpha_24,b_24,f_24,middle_index_24,x_0_24,scale_meas_13,scale_meas_24,nonpar_meas_13,nonpar_meas_24,angle_meas_13,angle_meas_24,offset_meas_13,offset_meas_24)

# Transform the strips on the board.
doublet_1.layer_1.strip_transform()
doublet_1.layer_2.strip_transform()

point_1_index = 287
point_2_index = 20
point_3_index = 27
point_4_index = 287

distance_between_strips = doublet_1.compare_strip_layers_alignment_side(point_1_index)
print("The misalignment between the strips at point 1 (alignment side) is %.3f microns." %distance_between_strips)

distance_between_strips = doublet_1.compare_strip_layers_alignment_side(point_2_index)
print("The misalignment between the strips at point 2 (alignment side) is %.3f microns." %distance_between_strips)

distance_between_strips = doublet_1.compare_strip_layers_non_alignment_side(point_3_index)
print("The misalignment between the strips at point 3 (non-alignment side) is %.3f microns." %distance_between_strips)

distance_between_strips = doublet_1.compare_strip_layers_non_alignment_side(point_4_index)
print("The misalignment between the strips at point 4 (non-alignment side) is %.3f microns." %distance_between_strips)

