# This transform and code were authored by Evan M. Carlson,
# University of Victoria / TRIUMF.
# Version 6.0 - December 21st, 2018

# This version adds in the quadruplet class, allowing for a full description
# of an individual quadruplet in terms of strip positions and alignment.
# A function that calculates the misalignment of any two given layer
# has been implemented. The Google sheets functionality remains.

# Some outstanding issues remain that will be addressed as soon as possible
# 1) I do not have access to all of the QC data, namely the Dagesh boards.
#    I will add this and upload a new version when I have that data
# 2) This does not address the issue of the stitch strip in the QS1.
#    I may not be able to appropriately resolve this issue with just the
#    info available from TriLabs. We may have to rely on Mikenberg's
#	 x-ray test, which is to be done at CERN.

#####################################################################
################ VERY IMPORTANT NOTES ON ORIENTATION ################
# All of this program is written with the orientation of the long   
# edge of the doublet being on the bottom. That is where the v      
# notch pin is located, and that is the center of the coordinate    
# system. 



import numpy as np
import numbers
import decimal
import scipy
import math
import sys
import matplotlib.pyplot as plt
import time

# Here's the stuff that's necessary for it to work with google sheets.
import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('stgc_client.json',scope)
client = gspread.authorize(creds)

write_sheet = client.open('Full_Calculator_Uncertainties').sheet1


############# BEGIN CLASS DEFINITIONS #############

class strip:

        # Initializer - Instance Attributes
        def __init__(self, y_coordinate, strip_number,scale,offset,nonpar,angle,offset_actual,n_strips,h,alpha,a,b,f,x_0,board_angle,is_first_half,board_type_arg):
                # Each strip will have a strip number and a nominal y coordinate.
                self.strip_number = strip_number
                self.y_coordinate = y_coordinate
                self.board_type = board_type_arg

                # Here are the nominal values for the strip and board.
                self.n_strips = n_strips
                self.h = h
                self.alpha = alpha
                self.a = a
                self.b = b
                self.f = f
                self.x_0 = x_0

                # Each strip will inherit the measurements from the board.
                self.scale = scale
                self.offset = offset
                self.nonpar = nonpar
                self.angle = angle
                self.offset_act = offset_actual
                self.is_first_half = is_first_half
                self.board_angle = board_angle

                # This creates the set of points that will make up the line. They all
                # nominally have the same y coordinate before being transformed.
                # NOTE: THE LEFT ENDPOINTS ARE ON THE ALIGNMENT SIDE OF THE BOARD. THIS IS VERY IMPORTANT
                # SO THE RIGHT ENDPOINTS ARE ON THE NOT ALIGNMENT SIDE. THIS IS  VERY IMPORTANT
                if self.board_type != 6:
                    self.left_endpoint = np.array([x_0 + (self.strip_number - 1) * strip_pitch * math.tan(self.board_angle * math.pi / 180.0), self.y_coordinate])
                    self.left_midpoint = np.array([a - alpha, self.y_coordinate])
                    self.centerpoint = np.array([a, self.y_coordinate])
                    self.right_midpoint = np.array([a + alpha, self.y_coordinate])
                    self.right_endpoint = np.array([2*a - x_0 -  (self.strip_number - 1) * strip_pitch * math.tan(self.board_angle * math.pi / 180.0), self.y_coordinate])
                    self.points = np.array([self.left_endpoint, self.left_midpoint, self.centerpoint, self.right_midpoint, self.right_endpoint])
                else:
                    if self.y_coordinate <= angle_start:
                        self.left_endpoint = np.array([x_0,self.y_coordinate])
                        self.left_midpoint = np.array([a - alpha, self.y_coordinate])
                        self.centerpoint = np.array([a, self.y_coordinate])
                        self.right_midpoint = np.array([a + alpha, self.y_coordinate])
                        self.right_endpoint = np.array([2*a - x_0, self.y_coordinate])
                        self.points = np.array([self.left_endpoint, self.left_midpoint, self.centerpoint, self.right_midpoint, self.right_endpoint])
                    else:
                        self.left_endpoint = np.array([x_0 + (self.y_coordinate - angle_start) * math.tan(self.board_angle * math.pi / 180.0),self.y_coordinate])
                        self.left_midpoint = np.array([a - alpha, self.y_coordinate])
                        self.centerpoint = np.array([a, self.y_coordinate])
                        self.right_midpoint = np.array([a + alpha, self.y_coordinate])
                        self.right_endpoint = np.array([2*a - x_0 - (self.y_coordinate - angle_start) * math.tan(self.board_angle * math.pi / 180.0), self.y_coordinate])
                        self.points = np.array([self.left_endpoint, self.left_midpoint, self.centerpoint, self.right_midpoint, self.right_endpoint])
                    
        # The function that transforms the points on the line. 
        def transform(self,x,y):
                if (self.board_type == 1) or (self.board_type == 2) or (self.board_type == 3):
                    if self.is_first_half == 0:
                        y_prime = y + self.offset_act + (self.scale / self.h)*(y - self.b) + (x - self.a) * math.tan((y - self.b) * (1.0+(self.scale/self.h)) / (self.scale+self.h) * math.atan(self.nonpar/(2.0*self.alpha))) - (x - self.a)*math.tan(self.angle*math.pi/180.0)
                    else:
                        y_prime = y + self.offset_act + (self.scale / self.h)*(y - self.b) - (x - self.a) * math.tan((y - self.b) * (1.0+(self.scale/self.h)) / (self.scale+self.h) * math.atan(self.nonpar/(2.0*self.alpha))) - (x - self.a)*math.tan(self.angle*math.pi/180.0)                    

                elif (self.board_type == 4) or (self.board_type == 5):
                    if self.is_first_half == 0:
                        y_prime = y + self.offset_act + (self.scale / self.h)*(y - self.b) + (x - self.a) * math.tan((y - self.b) * (1.0+(self.scale/self.h)) / (self.scale+self.h) * math.atan(self.nonpar/(2.0*self.alpha))) - (x - self.a)*math.tan(self.angle*math.pi/180.0)
                    else:
                        y_prime = y + self.offset_act + (self.scale / self.h)*(y - self.b) - (x - self.a) * math.tan((y - self.b) * (1.0+(self.scale/self.h)) / (self.scale+self.h) * math.atan(self.nonpar/(2.0*self.alpha))) - (x - self.a)*math.tan(self.angle*math.pi/180.0)
                elif board_type == 6:
                    if self.is_first_half == 0:
                        y_prime = y + self.offset_act + (self.scale / self.h)*(y - self.b) + (x - self.a) * math.tan((y - self.b) * (1.0+(self.scale/self.h)) / (self.scale+self.h) * math.atan(self.nonpar/(2.0*self.alpha))) - (x - self.a)*math.tan(self.angle*math.pi/180.0)
                    else:
                        y_prime = y + (y - self.f) * (self.scale / self.h) - (x - self.a) * math.tan(((y - self.f) / self.h) * math.atan(self.nonpar/(2.0*self.alpha))) - (x - self.a) * math.tan(self.angle * math.pi / 180.0) + self.offset - self.a * math.tan(self.angle * math.pi / 180.0)

                return x,y_prime


# The board class, which will have a number of strips on it.
class board:

        # Initializer - Instance Attributes
        def __init__(self, board_type_arg, scale_arg, offset_arg, nonpar_arg, angle_arg, is_first_half,n_strips,h_arg, alpha_arg, a_arg , b_arg , f_arg, middle_strip_index,x_0,board_angle):

                # Give the board the measurements and the board type.
                self.board_type = board_type_arg
                self.scale = scale_arg
                self.offset = offset_arg
                self.nonpar = nonpar_arg
                self.angle = angle_arg

                # Here are the nominal values for the board.
                self.n_strips = n_strips
                self.h = h_arg
                self.alpha = alpha_arg
                self.a = a_arg
                self.b = b_arg
                self.f = f_arg
                self.x_0 = x_0
                self.board_angle = board_angle

                # The relevant strip indices.
                self.bottom_strip_number = 5 +  is_first_half
                self.middle_strip_number = middle_strip_index

                # These indices work for QS3 boards, but I'm not sure if they hold for boards with an even number of strips.
                if is_first_half == 0:
                    self.top_strip_number = self.n_strips - self.bottom_strip_number - 2
                elif is_first_half == 1:
                    self.top_strip_number = self.n_strips - self.bottom_strip_number

                # Calculate the actual offset.
                if is_first_half == 0:
                    self.offset_actual = self.offset - (self.scale/self.h)*(self.f - self.b) - self.a * math.tan(self.angle * math.pi / 180.) + self.a * math.tan((self.f-self.b)*(1.+(self.scale/self.h))/(self.scale+self.h)* math.atan(self.nonpar/(2.*self.alpha)))
                else:
                    self.offset_actual = self.offset - (self.scale/self.h)*(self.f - self.b) - self.a * math.tan(self.angle * math.pi / 180.) - self.a * math.tan((self.f-self.b)*(1.+(self.scale/self.h))/(self.scale+self.h)* math.atan(self.nonpar/(2.*self.alpha)))

                # 1 means the first strip is a half strip, 0 is a full strip
                self.is_first_half = bool(is_first_half)
                self.strips = np.empty([n_strips], dtype = object)

                # This block will be used to differentiate between the 13 and 24 boards.
                if (self.board_type == 1) or (self.board_type == 2) or (self.board_type == 3):
                    if is_first_half == 0:
                        self.offset_actual = self.offset - (self.scale/self.h)*(self.f - self.b) - self.a * math.tan(self.angle * math.pi / 180.) + self.a * math.tan((self.f-self.b)*(1.+(self.scale/self.h))/(self.scale+self.h)* math.atan(self.nonpar/(2.*self.alpha)))
                    else:
                        self.offset_actual = self.offset - (self.scale/self.h)*(self.f - self.b) - self.a * math.tan(self.angle * math.pi / 180.) - self.a * math.tan((self.f-self.b)*(1.+(self.scale/self.h))/(self.scale+self.h)* math.atan(self.nonpar/(2.*self.alpha)))
                elif (self.board_type == 4) or (self.board_type == 5) or (self.board_type == 6):
                    if is_first_half == 0:
                        self.offset_actual = self.offset - (self.scale/self.h)*(self.f - self.b) - self.a * math.tan(self.angle * math.pi / 180.) - self.a * math.tan((self.f-self.b)*(1.+(self.scale/self.h))/(self.scale+self.h)* math.atan(self.nonpar/(2.*self.alpha)))
                    else:                        
                        self.offset_actual = self.offset - (self.scale/self.h)*(self.f - self.b) - self.a * math.tan(self.angle * math.pi / 180.) + self.a * math.tan((self.f-self.b)*(1.+(self.scale/self.h))/(self.scale+self.h)* math.atan(self.nonpar/(2.*self.alpha)))

                # Make an array of the endpoints which will be used for comparing to measurements.
                self.non_alignment_endpoints = np.empty([2,n_strips])
                self.alignment_endpoints = np.empty([2,n_strips])

                for i in range(0,n_strips):
                    # This is the x-coordinate. Note that calling the opposite endpoint is because of the flipped coordinate system
                    # FOR BOARDS, THE LEFT ENDPOINT IS NOT ALIGNMENT SIDE AND THE RIGHT ENDPOINT IS ALIGNMENT SIDE.
                    # THIS MATCHES THE PHYSICAL SETUP THAT WE HAVE. IF THE BOARD IS ORIENTED WITH THE LONG EDGE ON THE BOTTOM,
                    # THE ALIGNMENT FEATURES ARE ON THE RIGHT. SO THE BOARD RIGHT ENDPOINT IS ALIGNMENT SIDE
                    self.non_alignment_endpoints[0][i] = self.strips[i].right_endpoint[0]
                    self.alignment_endpoints[0][i] = self.strips[i].left_endpoint[0]
                    # This is the y-coordinate.
                    self.non_alignment_endpoints[1][i] = self.strips[i].right_endpoint[1]
                    self.alignment_endpoints[1][i] = self.strips[i].left_endpoint[1]



        # Define a method to look at a particular strip.
        def get_strip(self,number):
            return self.strips[number]


        ### NOTE THAT THE BOARD GEOMETRY WILL APPEAR FLIPPED ###
		### DUE TO PYTHON INTERPRETING POSITIVE X AS TO THE  ###
		### RIGHT, EVEN THOUGH IT IS TO THE LEFT FOR OUR     ###
		### SYSTEM. THIS IS ONLY AN ISSUE FOR THE DISPLAY,   ###
		### NOT THE CALCULATIONS.                            ###
        

        # A method to print the strips. Should yield a trapezoidal plot.
        def display_geometry(self):
            x_values = np.empty([5])
            y_values = np.empty([5])
            
            for i in range(0,n_strips):
                for j in range(0,5):
                    x_values[j] = self.get_strip(i).points[j][0]
                    y_values[j] = self.get_strip(i).points[j][1]

                plt.plot(x_values,y_values)

            plt.show()

        # A method to display only the relevant strips.
        def display_important_strips(self):
            x_values = np.empty([5])
            y_values = np.empty([5])

            important_strips = np.array([self.strips[self.bottom_strip_number],self.strips[self.middle_strip_number],self.strips[self.top_strip_number]])

            for item in important_strips:
                for j in range(0,5):
                    x_values[j] = item.points[j][0]
                    y_values[j] = item.points[j][1]
                plt.plot(x_values,y_values)

            plt.show()

         # A method to transform the strips.
        def strip_transform(self):

            # Loop through the strips and transform them one by one.
            for i in range(0,n_strips):
                index = 0
                while index < len(self.strips[i].points):
                    self.strips[i].points[index][0],self.strips[i].points[index][1] = self.strips[i].transform(self.strips[i].points[index][0],self.strips[i].points[index][1])
                    index += 1

            for i in range(0,n_strips):
                # This is the x-coordinate. Note that calling the right_endpoint is because of the flipped coordinate system
                self.non_alignment_endpoints[0][i] = self.strips[i].right_endpoint[0]
                self.alignment_endpoints[0][i] = self.strips[i].left_endpoint[0]
                # This is the y-coordinate.
                self.non_alignment_endpoints[1][i] = self.strips[i].right_endpoint[1]
                self.alignment_endpoints[1][i] = self.strips[i].left_endpoint[1]            


        # Here's a method to obtain the measurements.
        def get_measurements(self):
            # Calculate d1 and d2.
            # This if statement compensates for the fact that the 24 board is flipped over.
            if self.is_first_half == False:
                d_2_measurement = self.strips[self.top_strip_number].points[1][1] - self.strips[self.bottom_strip_number].points[1][1]
                d_1_measurement = self.strips[self.top_strip_number].points[3][1] - self.strips[self.bottom_strip_number].points[3][1]
            elif self.is_first_half == True:
                d_1_measurement = self.strips[self.top_strip_number].points[1][1] - self.strips[self.bottom_strip_number].points[1][1]
                d_2_measurement = self.strips[self.top_strip_number].points[3][1] - self.strips[self.bottom_strip_number].points[3][1]

            # Calculate all of the measurements for the board.
            scale_measurement = (d_1_measurement + d_2_measurement) / 2.0 - h
            nonpar_measurement = d_1_measurement - d_2_measurement
            if (self.board_type == 6) and (self.is_first_half == True):
                angle_measurement = math.atan((self.strips[self.middle_strip_number].points[0][1] - self.strips[self.middle_strip_number].points[4][1]) / (self.strips[self.middle_strip_number].points[4][0] - self.strips[self.middle_strip_number].points[0][0])) * 180.0 / math.pi
            else:
                angle_measurement = math.atan((self.strips[self.bottom_strip_number].points[0][1] - self.strips[self.bottom_strip_number].points[4][1]) / (self.strips[self.bottom_strip_number].points[4][0] - self.strips[self.bottom_strip_number].points[0][0])) * 180.0 / math.pi
            
            # Because the strips are not all the same length, we need to extrapolate the line of the middle strip
            # in order to get the offset measurement. To do this, we extend the line out from the edge
            # until it crosses our y axis directly above the pin.
            slope = (self.strips[self.middle_strip_number].points[2][1] - self.strips[self.middle_strip_number].points[1][1]) / (self.strips[self.middle_strip_number].points[2][0] - self.strips[self.middle_strip_number].points[1][0])
            offset_measurement = self.strips[self.middle_strip_number].points[1][1] - (self.a - self.alpha) * slope - self.f

            return scale_measurement, offset_measurement, nonpar_measurement, angle_measurement, d_1_measurement, d_2_measurement

# The class for the doublet, which will contain two board objects
class doublet:

        def __init__(self,board_type,board_angle,h_nom,a_nom,n_strips,alpha_13_nom,b_13_nom,f_13_nom,mid_index_13,x_0_13,alpha_24_nom,b_24_nom,f_24_nom,mid_index_24,x_0_24,scale_13,scale_24,nonpar_13,nonpar_24,angle_13,angle_24,offset_13,offset_24):

                # Give the doublet all of it's values.
                self.board_type = board_type
                self.board_angle = board_angle

                self.h_nom = h_nom
                self.a_nom = a_nom
                self.n_strips = n_strips

                self.alpha_13_nom = alpha_13_nom
                self.b_13_nom = b_13_nom
                self.f_13_nom = f_13_nom
                self.mid_index_13 = mid_index_13
                self.x_0_13 = x_0_13
                
                self.alpha_24_nom = alpha_24_nom
                self.b_24_nom = b_24_nom
                self.f_24_nom = f_24_nom
                self.mid_index_24 = mid_index_24
                self.x_0_24 = x_0_24

                self.scale_13 = scale_13
                self.nonpar_13 = nonpar_13
                self.angle_13 = angle_13
                self.offset_13 = offset_13

                self.scale_24 = scale_24
                self.nonpar_24 = nonpar_24
                self.angle_24 = angle_24
                self.offset_24 = offset_24

                # Create the boards that make up the doublet.
                self.layer_1 = board(self.board_type,self.scale_13,self.offset_13,self.nonpar_13,self.angle_13,0,self.n_strips,self.h_nom,self.alpha_13_nom,self.a_nom,self.b_13_nom,self.f_13_nom,self.mid_index_13,self.x_0_13,self.board_angle)
                self.layer_2 = board(self.board_type,self.scale_24,self.offset_24,self.nonpar_24,self.angle_24,1,self.n_strips,self.h_nom,self.alpha_24_nom,self.a_nom,self.b_24_nom,self.f_24_nom,self.mid_index_24,self.x_0_24,self.board_angle)

        # This function will compare the positions of the midpoint of adjacent strips in the two layers.
        # This function works for the nonalignment side.
        # They should be offset by approximately 1600 microns.
        def compare_strip_layers_non_alignment_side(self,strip_number):
            # Drop the strip number by 1, because I doubt they are using indexing from zero.
            index = strip_number - 1

            # This calls the right_endpoint of a strip object, which is thus the non_alignment side
            x_1 = self.layer_1.strips[index].points[4][0]
            x_2 = self.layer_2.strips[index].points[4][0]

            y_1 = self.layer_1.strips[index].points[4][1]
            y_2 = self.layer_2.strips[index].points[4][1]            

            distance = math.sqrt(math.pow((x_1 - x_2),2) + math.pow((y_1-y_2),2))
            nominal_distance = 1600.0 / math.cos(self.board_angle * math.pi / 180.0)
            difference = distance - nominal_distance
            # THE NEGATIVE ONE ACCOUNTS FOR THE FLIPPED PERSPECTIVE OF THE CAMERA TAKING THE MICROSCOPE PICTURES.
            # THIS NUMBER CAN BE DIRECTLY COMPARED TO THE NUMBER ON THE XML FILES. THIS SIGN CONVENTION
            # MAY BE DIFFERENT FOR DIFFERENT TEST SITES. BE CAREFUL.
            return -1.0 * difference

        # This function will compare the positions of the midpoint of adjacent strips in the two layers.
        # This function works for the alignment side.
        # They should be offset by approximately 1600 microns.
        def compare_strip_layers_alignment_side(self,strip_number):
            # Drop the strip number by 1, because I doubt they are using indexing from zero.
            index = strip_number - 1

            # This calls the left_endpoint of a strip object, which is thus the alignment side
            x_1 = self.layer_1.strips[index].points[0][0]
            x_2 = self.layer_2.strips[index].points[0][0]

            y_1 = self.layer_1.strips[index].points[0][1]
            y_2 = self.layer_2.strips[index].points[0][1]           

            # THIS RETURN VALUE MAY NEED TO BE MULTIPLIED BY NEGATIVE ONE 
            # DEPENDING ON THE SIGN CONVENTION OF THE TEST SITE. 
            distance = math.sqrt(math.pow((x_1 - x_2),2) + math.pow((y_1-y_2),2))
            nominal_distance = 1600.0 / math.cos(self.board_angle * math.pi / 180.0)
            difference = distance - nominal_distance
            return difference

class quadruplet:

        def __init__(self,board_type,board_angle,h_nom,a_nom,n_strips,alpha_13_nom,b_13_nom,f_13_nom,mid_index_13,x_0_13,alpha_24_nom,b_24_nom,f_24_nom,mid_index_24,x_0_24,scale_1,scale_2,scale_3,scale_4,nonpar_1,nonpar_2,nonpar_3,nonpar_4,angle_1,angle_2,angle_3,angle_4,offset_1,offset_2,offset_3,offset_4):

                # Give the doublet all of it's values.
                self.board_type = board_type
                self.board_angle = board_angle

                self.h_nom = h_nom
                self.a_nom = a_nom
                self.n_strips = n_strips

                self.alpha_13_nom = alpha_13_nom
                self.b_13_nom = b_13_nom
                self.f_13_nom = f_13_nom
                self.mid_index_13 = mid_index_13
                self.x_0_13 = x_0_13
                
                self.alpha_24_nom = alpha_24_nom
                self.b_24_nom = b_24_nom
                self.f_24_nom = f_24_nom
                self.mid_index_24 = mid_index_24
                self.x_0_24 = x_0_24

                self.scale_1 = scale_1
                self.nonpar_1 = nonpar_1
                self.angle_1 = angle_1
                self.offset_1 = offset_1

                self.scale_2 = scale_2
                self.nonpar_2 = nonpar_2
                self.angle_2 = angle_2
                self.offset_2 = offset_2

                self.scale_3 = scale_3
                self.nonpar_3 = nonpar_3
                self.angle_3 = angle_3
                self.offset_3 = offset_3

                self.scale_4 = scale_4
                self.nonpar_4 = nonpar_4
                self.angle_4 = angle_4
                self.offset_4 = offset_4

                # Create the doublets that make up the quadruplet
                self.doublet_12 = doublet(board_type,board_angle,h,a,n_strips,alpha_13,b_13,f_13,middle_index_13,x_0_13,alpha_24,b_24,f_24,middle_index_24,x_0_24,scale_1,scale_2,nonpar_1,nonpar_2,angle_1,angle_2,offset_1,offset_2)

                self.doublet_34 = doublet(board_type,board_angle,h,a,n_strips,alpha_13,b_13,f_13,middle_index_13,x_0_13,alpha_24,b_24,f_24,middle_index_24,x_0_24,scale_3,scale_4,nonpar_3,nonpar_4,angle_3,angle_4,offset_3,offset_4)

        def quad_transform(self):
            self.doublet_12.layer_1.strip_transform()
            self.doublet_12.layer_2.strip_transform()
            self.doublet_34.layer_1.strip_transform()
            self.doublet_34.layer_2.strip_transform()            

        def compare_strips_alignment_side(self, strip_number, first_layer, second_layer):
            # Drop the strip number by 1, because I doubt they are using indexing from zero.
            index = strip_number - 1

            if first_layer == 1:
                first_board = self.doublet_12.layer_1
            elif first_layer == 2:
                first_board = self.doublet_12.layer_2 
            elif first_layer == 3:
                first_board = self.doublet_34.layer_1
            else:
                first_board = self.doublet_34.layer_2

            if second_layer == 1:
                second_board = self.doublet_12.layer_1
            elif second_layer == 2:
                second_board = self.doublet_12.layer_2 
            elif second_layer == 3:
                second_board = self.doublet_34.layer_1
            else:
                second_board = self.doublet_34.layer_2

            x_1 = first_board.strips[index].points[0][0]
            x_2 = second_board.strips[index].points[0][0]

            y_1 = first_board.strips[index].points[0][1]
            y_2 = second_board.strips[index].points[0][1]

            if math.fabs(first_layer - second_layer) % 2 == 0:
                nominal_distance = 0.0
            else:
                nominal_distance = 1600.0 / math.cos(self.board_angle * math.pi / 180.0)

            distance = math.sqrt(math.pow((x_1 - x_2),2) + math.pow((y_1-y_2),2))
            difference = distance - nominal_distance

            alignment_uncertainty = misalignment_uncertainty_alignment_side(first_board.strips[index],second_board.strips[index])

            return difference, alignment_uncertainty

        def compare_strips_non_alignment_side(self, strip_number, first_layer, second_layer):
            # Drop the strip number by 1, because I doubt they are using indexing from zero.
            index = strip_number - 1

            if first_layer == 1:
                first_board = self.doublet_12.layer_1
            elif first_layer == 2:
                first_board = self.doublet_12.layer_2 
            elif first_layer == 3:
                first_board = self.doublet_34.layer_1
            else:
                first_board = self.doublet_34.layer_2

            if second_layer == 1:
                second_board = self.doublet_12.layer_1
            elif second_layer == 2:
                second_board = self.doublet_12.layer_2 
            elif second_layer == 3:
                second_board = self.doublet_34.layer_1
            else:
                second_board = self.doublet_34.layer_2

            x_1 = first_board.strips[index].points[4][0]
            x_2 = second_board.strips[index].points[4][0]

            y_1 = first_board.strips[index].points[4][1]
            y_2 = second_board.strips[index].points[4][1]

            if math.fabs(first_layer - second_layer) % 2 == 0:
                nominal_distance = 0.0
            else:
                nominal_distance = 1600.0 / math.cos(self.board_angle * math.pi / 180.0)

            distance = math.sqrt(math.pow((x_1 - x_2),2) + math.pow((y_1-y_2),2))
            difference = distance - nominal_distance

            alignment_uncertainty = misalignment_uncertainty_non_alignment_side(first_board.strips[index],second_board.strips[index])

            return -1.0 * difference, alignment_uncertainty



############# END CLASS DEFINITIONS #############

########### ERROR PROPAGATION FUNCTION ###########

def strip_uncertainty_alignment_side(strip_1):

    s_unc = 30.0
    np_unc = 30.0
    offset_unc = 30.0
    angle_unc = 0.0000175

    s_term = ((strip_1.y_coordinate-strip_1.b)/strip_1.h - (strip_1.f-strip_1.b)/strip_1.h)*s_unc
    offset_term = offset_unc
    angle_term = ((strip_1.left_endpoint[0] - strip_1.a) * math.pow(math.cos((strip_1.angle)),-2) + strip_1.a * math.pow(math.cos((strip_1.angle)),-2)) * angle_unc

    np_term = ((1 / (2 * strip_1.alpha * strip_1.h)) * (1 / (1 + math.pow(strip_1.nonpar / (2 * strip_1.alpha),2))) * ((strip_1.left_endpoint[0] - strip_1.a) * (strip_1.y_coordinate-strip_1.b) * math.pow(math.cos((strip_1.y_coordinate - strip_1.b) / strip_1.h * math.atan(strip_1.nonpar / (2 * strip_1.alpha))),-2) + strip_1.a * (strip_1.f - strip_1.b) * math.pow(math.cos((strip_1.f - strip_1.b) / strip_1.h * math.atan(strip_1.nonpar / (2 * strip_1.alpha))),-2))) * np_unc

    net_uncertainty = math.sqrt(math.pow(s_term,2) + math.pow(offset_term,2) + math.pow(angle_term,2) + math.pow(np_term,2))

    return net_uncertainty

def strip_uncertainty_non_alignment_side(strip_1):

    s_unc = 30.0
    np_unc = 30.0
    offset_unc = 30.0
    angle_unc = 0.0000175

    s_term = ((strip_1.y_coordinate-strip_1.b)/strip_1.h - (strip_1.f-strip_1.b)/strip_1.h)*s_unc
    offset_term = offset_unc
    angle_term = ((strip_1.right_endpoint[0] - strip_1.a) * math.pow(math.cos((strip_1.angle)),-2) + strip_1.a * math.pow(math.cos((strip_1.angle)),-2)) * angle_unc

    np_term = ((1 / (2 * strip_1.alpha * strip_1.h)) * (1 / (1 + math.pow(strip_1.nonpar / (2 * strip_1.alpha),2))) * ((strip_1.right_endpoint[0] - strip_1.a) * (strip_1.y_coordinate-strip_1.b) * math.pow(math.cos((strip_1.y_coordinate - strip_1.b) / strip_1.h * math.atan(strip_1.nonpar / (2 * strip_1.alpha))),-2) + strip_1.a * (strip_1.f - strip_1.b) * math.pow(math.cos((strip_1.f - strip_1.b) / strip_1.h * math.atan(strip_1.nonpar / (2 * strip_1.alpha))),-2))) * np_unc

    net_uncertainty = math.sqrt(math.pow(s_term,2) + math.pow(offset_term,2) + math.pow(angle_term,2) + math.pow(np_term,2))

    return net_uncertainty

def misalignment_uncertainty_alignment_side(strip_1,strip_2):
    strip_1_error = strip_uncertainty_alignment_side(strip_1)
    strip_2_error = strip_uncertainty_alignment_side(strip_2)

    misalignment_unc = math.sqrt((math.pow(strip_1_error,2) + math.pow(strip_2_error,2)) * math.pow((strip_1.y_coordinate - strip_2.y_coordinate),2) * math.pow(math.pow(strip_1.left_endpoint[0] - strip_2.left_endpoint[0],2) + math.pow(strip_1.y_coordinate - strip_2.y_coordinate,2),-1))

    return misalignment_unc

def misalignment_uncertainty_non_alignment_side(strip_1,strip_2):
    strip_1_error = strip_uncertainty_non_alignment_side(strip_1)
    strip_2_error = strip_uncertainty_non_alignment_side(strip_2)

    misalignment_unc = math.sqrt((math.pow(strip_1_error,2) + math.pow(strip_2_error,2)) * math.pow((strip_1.y_coordinate - strip_2.y_coordinate),2) * math.pow(math.pow(strip_1.right_endpoint[0] - strip_2.right_endpoint[0],2) + math.pow(strip_1.y_coordinate - strip_2.y_coordinate,2),-1))

    return misalignment_unc

print("This program assembles the strip geometry of an sTGC quadruplet based on the provided measurements.")

# Get the type of the board from the user.

board_type = input("What type of board are we working with? Enter 1 for QS1, 2 for QS2, 3 for QS3, 4 for QL1, 5 for QL2, and 6 for QL3. ")


while(board_type != 1 and board_type != 2 and board_type != 3 and board_type != 4 and board_type != 5 and board_type != 6):
        board_type = input("Please input a valid board type. ")

# Set the appropriate variables based on the board type.

# The QS1 presents a different case because the board was etched in two separate instances. 
# The measurements that were taken may not be of much help because of this issue.
if board_type == 1:
        n_strips = 406
        h = 1292800.0
        a = 371853.4
        board_angle = 8.5
        
        # NOTE: Need to fix the b values for this because they are measured from the first full strip.
        b_13 = 16299.1
        f_13 = 646699.1
        alpha_13 = 163498
        middle_index_13 = 202
        x_0_13 = 2634.6

        b_24 = 17899.1
        f_24 = 648299.1
        alpha_24 = 163259.4
        middle_index_24 = 203
        x_0_24 = 2400.76

        sh = client.open('Copy of GS1 General Summary')

        sheet_13 = sh.get_worksheet(4)
        sheet_24 = sh.get_worksheet(5)

elif board_type == 2:
        n_strips = 365
        h = 1129600.0
        a = 362656.9
        board_angle = 8.5

        b_13 = 13299.1
        f_13 = 579699.1
        alpha_13 = 362656.9
        middle_index_13 = 182
        x_0_13 = 2186.34

        b_24 = 14899.1
        f_24 = 578099.1
        alpha_24 = 362418.2
        middle_index_24 = 182
        x_0_24 = 1952.46

        sh = client.open('Copy of GS2 General Summary')

        sheet_13 = sh.get_worksheet(2)
        sheet_24 = sh.get_worksheet(3)

elif board_type == 3:
        n_strips = 307
        h = 944000.0
        a = 703127.3
        board_angle = 8.5
        
        b_13 = 13299.1
        f_13 = 486899.1
        alpha_13 = 542566.4
        middle_index_13 = 153
        x_0_13 = 2186.34

        b_24 = 14899.1
        f_24 = 485299.1
        alpha_24 = 542327.7
        middle_index_24 = 153
        x_0_24 = 1952.46

        sh = client.open('Copy of GS3 General Summary')

        sheet_13 = sh.get_worksheet(4)
        sheet_24 = sh.get_worksheet(5)

elif board_type == 4:
        n_strips = 408
        h = 1267200.0
        a = 602161.1
        board_angle = 14.0
        
        b_13 = 16107.6
        f_13 = 649707.6
        alpha_13 = 264522.3
        middle_index_13 = 203
        x_0_13 = 2758.45

        b_24 = 17707.6
        f_24 = 651307.6
        alpha_24 = 264124.2
        middle_index_24 = 204
        x_0_24 = 2383.2

        sheet_name = 'Copy of GL1 General Summary'

elif board_type == 5:
        n_strips = 366
        h = 1132800.0
        a = 901754.3
        board_angle = 14.0
        
        b_13 = 13107.6
        f_13 = 579507.6
        alpha_13 = 598373.3
        middle_index_13 = 182
        x_0_13 = 2010.45

        b_24 = 14707.6
        f_24 = 581107.6
        alpha_24 = 597975.1
        middle_index_24 = 183
        x_0_24 = 1635.3

        sh = client.open('Copy of GL2 General Summary')

        sheet_13 = sh.get_worksheet(4)
        sheet_24 = sh.get_worksheet(5)

elif board_type == 6:
        n_strips = 353
        h = 1091200.0
        a = 1058035.5
        board_angle = 14.0
        
        b_13 = 13600.0
        f_13 = 560800.0
        alpha_13 = 899313.0
        middle_index_13 = 176
        x_0_13 = 2535.5

        b_24 = 15200.0
        f_24 = 559200.0
        alpha_24 = 898914.7
        middle_index_24 =  176
        x_0_24 = 2535.5
        angle_start = 538774.3

        sh = client.open('Copy of GL3 General Summary')

        sheet_13 = sh.get_worksheet(2)
        sheet_24 = sh.get_worksheet(3)


doublets_list = ['DS3.P12.1','DS3.P12.2','DS3.P12.3','DS3.P12.4','DS3.P12.5','DS3.P12.6','DS3.P12.7','DS3.P12.8','DS3.P12.9','DS3.P12.10','DS3.P12.11','DS3.P34.1','DS3.P34.2','DS3.P34.3','DS3.P34.4','DS3.P34.5','DS3.P34.6','DS3.P34.7','DS3.P34.8','DS3.P34.9','DS3.P34.10','DS3.P34.11','DS3.P34.12']

doublet_13_serial = [3,6,13,8,21,23,31,19,39,37,35,2,9,16,4,24,28,40,18,38,30,36,32,20]
doublet_24_serial = [7,4,11,6,25,29,15,23,39,41,9,3,18,38,8,44,40,22,12,26,28,51,52,24]

point_1_meas = [-47,-28,1,-48,-99,102,328,-27,113,105,82,20,-5,98,97,177,76,26,82,124,102,68,163]
point_1_unc = [10,19,56,25,42,28,27,17,13,29,34,24,30,20,30,40,27,26,30,22,17,20,17]

point_2_meas = [-112,-143,85,-85,-143,-5,232,-149,15,16,-102,-34,-21,9,19,-11,-165,-27,89,-27,-4,-81,118]
point_2_unc = [19,27,57,20,42,29,24,36,36,24,22,26,42,55,44,44,42,22,59,24,17,57,13]

point_3_meas = [205,168,53,156,146,-46,18,225,-10,-34,21,-104,133,-60,-8,89,125,126,50,143,62,89,-31]
point_3_unc = [17,32,74,30,34,24,22,36,21,23,27,19,16,45,18,46,4,19,22,18,52,67,16]

point_4_meas = [66,164,86,82,5,-57,-169,149,-165,-234,-112,-102,78,-138,80,-105,-3,71,1,-4,-118,-136,-168]
point_4_unc = [21,43,44,47,14,31,30,23,14,14,22,27,17,26,60,49,16,29,31,16,46,25,19]

# We want to start putting our values in at row 2 of the Google Sheet
row_index = 2

for doublet_index in range(0,len(doublets_list)):

    # This block of code will obtain the values of the 13 board
    # measurements from the user. All of the values will be in microns
    # The scale, offset, nonparallelism and angle are all entered
    # as the deviations from the nominal.

    doublet_serial = doublets_list[doublet_index]

    print(doublet_serial)


#    print("This first set of measurements is for the 13 board.")
    serial_13 = doublet_13_serial[row_index - 2]

    scale_meas_13 = sheet_13.cell(8, serial_13 + 1).value

    offset_meas_13 = sheet_13.cell(10,serial_13 + 1).value

    nonpar_meas_13 = sheet_13.cell(9,serial_13 + 1).value

    angle_meas_13 = sheet_13.cell(11,serial_13 + 1).value




    # Do you have the d1 and d2 measurements? If so, you can input them here.
    # The program will then compare them later after the transform
    # include_d1_d2_13 = input("Do you have the D1 and D2 measurements? 1 for yes, 0 for no. ")

    # while(include_d1_d2_13 != 1 and include_d1_d2_13 != 0):
    #         include_d1_d2_13 = input("Please input a valid option. ")


    # Take in the measurements if the user has them. 
    # if include_d1_d2_13 == 1:
    #     d1_meas_13 = input("What is the d1 value in microns? ")
    #     d2_meas_13 = input("What is the d2 value in microns? ")


    # Now we will take in the measurements for the 24 board.

#    print("This set of measurements is for the 24 board.")
    serial_24 = doublet_24_serial[row_index - 2]

    scale_meas_24 = sheet_24.cell(8,serial_24 + 1).value

    offset_meas_24 = sheet_24.cell(10,serial_24 + 1).value

    nonpar_meas_24 = sheet_24.cell(9,serial_24 + 1).value

    angle_meas_24 = sheet_24.cell(11,serial_24 + 1).value


    # Do you have the d1 and d2 measurements? If so, you can input them here.
    # The program will then compare them later after the transform
    # include_d1_d2_24 = input("Do you have the D1 and D2 measurements? 1 for yes, 0 for no. ")

    # while(include_d1_d2_24 != 1 and include_d1_d2_24 != 0):
    #         include_d1_d2_24 = input("Please input a valid option. ")


    # Take in the measurements if the user has them. 
    # if include_d1_d2_24 == 1:
    #     d1_meas_24 = input("What is the d1 value in microns? ")
    #     d2_meas_24 = input("What is the d2 value in microns? ")

    # The strip pitch in microns.
    strip_pitch = 3200


    # Cast the 13 data types.
    # The values out of Google Sheets are unicode. You must convert them to a string and then a float to be able to use them.
    scale_meas_13 = scale_meas_13.encode('utf-8')
    offset_meas_13 = offset_meas_13.encode('utf-8')
    nonpar_meas_13 = nonpar_meas_13.encode('utf-8')
    angle_meas_13 = angle_meas_13.encode('utf-8')

    scale_meas_13 = float(scale_meas_13)
    offset_meas_13 = float(offset_meas_13)
    nonpar_meas_13 = float(nonpar_meas_13)
    angle_meas_13 = float(angle_meas_13)

    # Convert to microns 
    scale_meas_13 = 1000 * scale_meas_13
    offset_meas_13 = 1000 * offset_meas_13
    nonpar_meas_13 = 1000 * nonpar_meas_13

    # Print out the values of the 13 measurements for the user.
    # print ("For the 13 board, the measurements are:")
    # print ("Scale = %3.1f " %scale_meas_13)
    # print ("Offset = %3.1f " %offset_meas_13)
    # print ("Nonparallelism = %3.1f " %nonpar_meas_13)
    # print ("Angle = %1.4f " %angle_meas_13)

    # Cast the 24 data types.
    # The values out of Google Sheets are unicode. You must convert them to a string and then a float to be able to use them.
    scale_meas_24 = scale_meas_24.encode('utf-8')
    offset_meas_24 = offset_meas_24.encode('utf-8')
    nonpar_meas_24 = nonpar_meas_24.encode('utf-8')
    angle_meas_24 = angle_meas_24.encode('utf-8')

    scale_meas_24 = float(scale_meas_24)
    offset_meas_24 = float(offset_meas_24)
    nonpar_meas_24 = float(nonpar_meas_24)
    angle_meas_24 = float(angle_meas_24)

    # Convert to microns
    scale_meas_24 = 1000 * scale_meas_24
    offset_meas_24 = 1000 * offset_meas_24
    nonpar_meas_24 = 1000 * nonpar_meas_24

    # Print out the values of the 24 measurements for the user.
    # print ("For the 24 board, the measurements are:")
    # print ("Scale = %3.1f " %scale_meas_24)
    # print ("Offset = %3.1f " %offset_meas_24)
    # print ("Nonparallelism = %3.1f " %nonpar_meas_24)
    # print ("Angle = %1.4f " %angle_meas_24)


    # Now we will define a 13 board with the parameters given by the user.
    quadruplet_1 = quadruplet(board_type,board_angle,h,a,n_strips,alpha_13,b_13,f_13,middle_index_13,x_0_13,alpha_24,b_24,f_24,middle_index_24,x_0_24,scale_meas_13,scale_meas_24,scale_meas_13,scale_meas_24,nonpar_meas_13,nonpar_meas_24,nonpar_meas_13,nonpar_meas_24,angle_meas_13,angle_meas_24,angle_meas_13,angle_meas_24,offset_meas_13,offset_meas_24,offset_meas_13,offset_meas_24)
    # Transform the strips on the board.
    quadruplet_1.quad_transform()

    point_1_index = 287
    point_2_index = 20
    point_3_index = 27
    point_4_index = 287

    distance_between_strips_1,unc_1 = quadruplet_1.compare_strips_alignment_side(point_1_index,1,2)
    print("The misalignment between the strips at Point 1 is " + str(distance_between_strips_1) + " +- " + str(unc_1))

    distance_between_strips_2,unc_2 = quadruplet_1.compare_strips_alignment_side(point_2_index,1,2)
    print("The misalignment between the strips at Point 2 is " + str(distance_between_strips_2) + " +- " + str(unc_2))

    distance_between_strips_3,unc_3 = quadruplet_1.compare_strips_non_alignment_side(point_3_index,1,2)
    print("The misalignment between the strips at Point 3 is " + str(distance_between_strips_3) + " +- " + str(unc_3))

    distance_between_strips_4,unc_4 = quadruplet_1.compare_strips_non_alignment_side(point_4_index,1,2)
    print("The misalignment between the strips at Point 4 is " + str(distance_between_strips_4) + " +- " + str(unc_4))

    write_sheet.insert_row([doublets_list[row_index - 2],doublet_13_serial[row_index - 2],doublet_24_serial[row_index - 2],scale_meas_13,offset_meas_13,nonpar_meas_13,angle_meas_13,scale_meas_24,offset_meas_24,nonpar_meas_24,angle_meas_24,point_1_meas[row_index - 2],point_1_unc[row_index - 2],distance_between_strips_1,unc_1,point_2_meas[row_index - 2],point_2_unc[row_index - 2],distance_between_strips_2,unc_2,point_3_meas[row_index - 2],point_3_unc[row_index - 2],distance_between_strips_3,unc_3,point_4_meas[row_index - 2],point_4_unc[row_index - 2],distance_between_strips_4,unc_4],index = row_index)

    row_index = row_index + 1

    # Apparently the Google Sheets API only allows you to read from it 100 times in a 100 second interval.
    # So to avoid the quota being reached I have it sleep for five seconds on each doublet. 
    print("Sleepy time!")
    time.sleep(5)

### END OF PROGRAM ###